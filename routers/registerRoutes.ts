import express, { Request, Response } from 'express';
import { client } from '../main';
import { hashPassword } from '../hash';

export const registerRoutes = express.Router();

//Handle Local account registration
registerRoutes.post('/', async (req: Request, res: Response) => {
    try {
        const { name, email, password } = req.body;

        //Check if the email is registered or not
        const selectQuery = 'SELECT * FROM local_account WHERE username = $1';
        const existUser = await client.query(selectQuery, [email]);

        if (existUser.rows[0]) {
            res.status(400).json({
                message: 'This email has already been registered!',
            });
            return;
        }

        //Create new user
        const query =
            'INSERT INTO users(profile_name) VALUES ($1) RETURNING id';

        const user = await client.query(query, [name]);

        //Create new local account
        const queryLocal =
            'INSERT INTO local_account(user_id, username, hashed_password) VALUES ($1, $2, $3)';

        const hashedPassword = await hashPassword(password);

        await client.query(queryLocal, [
            user.rows[0].id,
            email,
            hashedPassword,
        ]);

        //Add user id in our session
        req.session['user'] = {
            id: user.rows[0].id,
        };

        res.json({ message: 'successful' });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error.⛔️' });
    }
});

//Handle Google account registration
registerRoutes.post('/google', async (req: Request, res: Response) => {
    try {
        const { email, username } = req.body;

        //Create new user
        const query =
            'INSERT INTO users(profile_name) VALUES ($1) RETURNING id';

        const user = await client.query(query, [username]);

        //Create new google account
        const queryLocal =
            'INSERT INTO google_account(user_id, email) VALUES ($1, $2)';

        await client.query(queryLocal, [user.rows[0].id, email]);

        //Add user id in our session
        req.session['user'] = {
            id: user.rows[0].user_id,
        };

        res.json({ message: 'successful' });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error.⛔️' });
    }
});
