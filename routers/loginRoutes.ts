import express, { Request, Response } from 'express';
import { client } from '../main';
import { checkPassword } from '../hash';
import fetch from 'node-fetch';

export const loginRoutes = express.Router();

//Handle Local Login
loginRoutes.post('/', async (req: Request, res: Response) => {
    try {
        const username = req.body.username;
        const password = req.body.password;

        console.log(username, password);

        //Check if username exist in DB
        const checkQuery =
            'SELECT COUNT(*) FROM local_account WHERE username = $1';
        const checkUser = await client.query(checkQuery, [username]);

        if (checkUser.rows[0]['count'] == 0) {
            res.status(400).json({ message: 'Invalid username or password.' });
            return;
        }

        const query = 'SELECT * FROM local_account WHERE username = $1';
        const user = await client.query(query, [username]);

        //Check if password exist in DB
        const match = await checkPassword(
            password,
            user.rows[0].hashed_password
        );

        if (!match) {
            res.status(400).json({ message: 'Invalid username or password.' });
            return;
        }

        //Add user id in our session
        req.session['user'] = {
            id: user.rows[0].user_id,
        };

        res.json({ id: user.rows[0].user_id });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error.⛔️' });
    }
});

//Handle Google Login
loginRoutes.get('/google', async (req, res) => {
    try {
        //Receive access token
        const accessToken = req.session?.['grant'].response.access_token;
        //Fetching user information
        const fetchRes = await fetch(
            'https://www.googleapis.com/oauth2/v2/userinfo',
            {
                method: 'get',
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            }
        );
        const result = await fetchRes.json();

        //Check if email exist in our DB
        const user = (
            await client.query(
                `SELECT * FROM google_account WHERE google_account.email = $1`,
                [result.email]
            )
        ).rows[0];

        //Register for an account if no user was found
        if (!user) {
            await fetch('http://localhost:8080/register/google', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                body: JSON.stringify({
                    email: result.email,
                    username: result.name,
                }),
            });

            res.redirect('/index.html');
            return;
        }

        //Add user id in our session
        if (req.session) {
            req.session['user'] = {
                id: user.user_id,
            };
        }

        return res.redirect('/index.html');
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error.⛔️' });
    }
});

// Logout
loginRoutes.delete('/', async (req: Request, res: Response) => {
    try {
        //Remove user session
        if (req.session) {
            req.session.destroy((err) => {
                if (err) {
                    res.status(400).send('Unable to logout');
                } else {
                    res.send('Logout successful');
                }
            });
        } else {
            res.end();
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error.⛔️' });
    }
});
