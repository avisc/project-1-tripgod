/**
 * This is the /itinerary route
 *
 * GET /user/:id
 * Fetch any user itinerary entry list array
 *
 * GET /user/:id/all
 * Fetch any user itinerary all entries data
 *
 * GET /all
 * Fetch all itinerary entries data
 *
 * GET /bookmark
 * Fetch all itinerary entries data
 *
 * GET /:id
 * Fetch itinerary entry data
 *
 * POST /
 * Create new itinerary entry
 *
 * POST /bookmark/:id
 * Insert bookmark
 *
 * PUT /:id
 * Update itinerary entry info
 *
 * DELETE /bookmark/:id
 * Delete bookmark
 *
 * DELETE /:id
 * Delete itinerary entry
 *
 */

import express, { Request, Response, NextFunction } from 'express';
import { QueryResult } from 'pg';
import { client } from '../main';
import { Itinerary } from '../model';

export const itineraryRoutes = express.Router();
itineraryRoutes.post('/member', getMembers);

async function getMembers(req: Request, res: Response) {}

//Create new itinerary member for owner
itineraryRoutes.post('/member', async (req, res) => {
    try {
        const userID = req.session['user'].id;
        const itineraryID = req.body.itinerary_id;

        const query =
            'INSERT INTO itinerary_member (itinerary_id, user_id) VALUES ($1, $2) RETURNING *';

        const result = await client.query(query, [itineraryID, userID]);

        res.json(result.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Invite new member
itineraryRoutes.post('/member/:id', async (req, res) => {
    try {
        const userID = req.params.id;
        const ownerID = req.body.ownerID;
        const itineraryID = req.body.itinerary_id;

        if (!req.session['user']) {
            res.status(401).json({ message: 'Login to continues' });
            return;
        }

        //Check if it is the owner of the itinerary
        if (req.session) {
            if (ownerID !== req.session['user'].id) {
                res.status(401).json({ message: 'Login to continues' });
                return;
            }
        }

        //Check if user already is the member
        const member =
            'SELECT COUNT(*) FROM itinerary_member WHERE user_id = $1 AND itinerary_id = $2';
        const haveMember = await client.query(member, [userID, itineraryID]);

        console.log(haveMember.rows[0].count);

        if (haveMember.rows[0].count != 0) {
            res.status(400).json({ message: 'User is already a member' });
            return;
        }

        //Check if user exist
        const queryForUser = 'SELECT * FROM users WHERE id = $1';
        const user = await client.query(queryForUser, [userID]);

        if (user.rows.length === 0) {
            res.status(400).json({ message: 'User not found' });
            return;
        }

        //Create new member
        const query =
            'INSERT INTO itinerary_member (itinerary_id, user_id) VALUES ($1, $2) RETURNING *';

        const result = await client.query(query, [itineraryID, userID]);

        res.json(result.rows[0]);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Delete member
itineraryRoutes.delete('/member/:username', async (req, res) => {
    try {
        const userID = req.params.username;
        const itineraryID = req.body.itinerary_id;
        const ownerID = req.body.ownerID;

        if (userID === ownerID) {
            res.status(400).json({
                message: `Can't delete the owner`,
            });
            return;
        }

        //Check if owner has logged in
        if (!req.session['user']) {
            res.status(401).json({ message: 'Login to continues' });
            return;
        }

        //Delete member
        const deleteMember =
            'DELETE FROM itinerary_member WHERE user_id = $1 AND itinerary_id = $2 RETURNING *';

        await client.query(deleteMember, [userID, itineraryID]);

        res.json({ message: 'successful' });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get member of an itinerary
itineraryRoutes.get('/member/:itinerary_id', async (req, res) => {
    try {
        const itineraryID = req.params.itinerary_id;
        const userID = req.session['user'].id;

        const query = 'SELECT * FROM itinerary_member WHERE itinerary_id = $1';

        const result = await client.query(query, [itineraryID]);

        const members = result.rows;

        const isMember = members.some((member) => {
            return member.user_id === userID;
        });

        if (!isMember) {
            res.status(400).json({
                message: `user is not the member of this itinerary`,
            });
            return;
        } else {
            res.json(members);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get Profile name of the members
itineraryRoutes.get('/member/username/:itinerary_id', async (req, res) => {
    try {
        const itineraryID = req.params.itinerary_id;

        const query =
            'SELECT * From itinerary_member INNER JOIN users ON user_id = users.id WHERE itinerary_id = $1';

        const result = await client.query(query, [itineraryID]);

        res.json(result.rows);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Get any user itinerary list array
itineraryRoutes.get('/user/:id', async (req, res) => {
    try {
        const userId = Number(req.params.id) || req.session['user'].id;
        const result = await client.query(
            `SELECT id, name FROM itinerary WHERE owner_id = $1 AND
            CASE
            WHEN owner_id != $2 THEN public = true
            ELSE true
            END
            ORDER BY id DESC`,
            [userId, req.session['user']?.id]
        );

        {
            res.json(result.rows);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Get any user itinerary details
itineraryRoutes.get('/user/:id/all', async (req, res) => {
    try {
        const itineraryList = await client.query(
            `SELECT itinerary.id, itinerary.name, itinerary.days, itinerary.owner_id, 
            users.profile_name, users.photo, 
            bookmarked.count AS bookmark
            FROM itinerary 
            INNER JOIN users ON itinerary.owner_id = users.id
            LEFT OUTER JOIN 
            (SELECT itinerary_id, COUNT(*) FROM bookmark WHERE user_id = $1 GROUP BY itinerary_id) AS bookmarked
            ON itinerary.id = bookmarked.itinerary_id
            WHERE itinerary.public = TRUE
            AND users.id = $2
            ORDER BY itinerary.id DESC`,
            [req.session['user']?.id, req.params.id]
        );
        if (!itineraryList.rows[0]) {
            res.status(400).json({ message: 'Itinerary not found' });
        } else {
            const itineraryWithEvents = await Promise.all(
                itineraryList.rows.map(async (itinerary) => {
                    // add bookmark count to result
                    const bookmarkCount = await client.query(
                        `SELECT COUNT(*) FROM bookmark WHERE itinerary_id = ${itinerary.id}`
                    );
                    itinerary.bookmarkCount = bookmarkCount.rows[0].count;
                    // add itinerary events to an array
                    itinerary.events = [];
                    const itineraryEvents = await client.query(
                        `SELECT itinerary_event.name FROM itinerary_event
                    WHERE type = 'main' AND itinerary_id = ${itinerary.id}`
                    );
                    for (let event of itineraryEvents.rows) {
                        itinerary.events.push(event.name);
                    }
                    // add destinations to an array
                    itinerary.destinations = [];
                    const destinations = await client.query(
                        `SELECT destination_group.city, destination_group.country 
                        FROM destination_group
                        INNER JOIN destination
                        ON destination_group.id = destination.destination_group_id
                        WHERE itinerary_id = ${itinerary.id}`
                    );
                    for (let destination of destinations.rows) {
                        itinerary.destinations.push(destination);
                    }
                    return itinerary;
                })
            );
            res.json(itineraryWithEvents);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Get all itinerary details
itineraryRoutes.get('/all', async (req, res) => {
    try {
        const itineraryList = await client.query(
            `SELECT itinerary.id, itinerary.name, 
            itinerary.days, itinerary.owner_id, 
            users.profile_name, users.photo, 
            bookmarked.count AS bookmark, 
            totalBookmark.count AS totalBookmark
            FROM itinerary 
            INNER JOIN users 
            ON itinerary.owner_id = users.id
            LEFT OUTER JOIN 
            (SELECT itinerary_id, COUNT(*) FROM bookmark WHERE user_id = $1 GROUP BY itinerary_id) AS bookmarked
            ON itinerary.id = bookmarked.itinerary_id
            LEFT OUTER JOIN (SELECT itinerary_id, COUNT(*) FROM bookmark GROUP BY itinerary_id) AS totalBookmark
            ON itinerary.id = totalBookmark.itinerary_id
            WHERE itinerary.public = TRUE
            ORDER BY
            CASE 
                WHEN $2 = 'bookmark' THEN totalBookmark.count
                ELSE itinerary.id
            END
            DESC NULLS LAST`,
            [req.session['user']?.id, req.query.sort]
        );
        if (!itineraryList.rows[0]) {
            res.status(400).json({ message: 'Itinerary not found' });
        } else {
            const itineraryWithEvents = await Promise.all(
                itineraryList.rows.map(async (itinerary) => {
                    // add itinerary events to an array
                    itinerary.events = [];
                    const itineraryEvents = await client.query(
                        `SELECT itinerary_event.name FROM itinerary_event
                    WHERE type = 'main' AND itinerary_id = ${itinerary.id}`
                    );
                    for (let event of itineraryEvents.rows) {
                        itinerary.events.push(event.name);
                    }
                    // add destinations to an array
                    itinerary.destinations = [];
                    const destinations = await client.query(
                        `SELECT destination_group.city, destination_group.country 
                        FROM destination_group
                        INNER JOIN destination
                        ON destination_group.id = destination.destination_group_id
                        WHERE itinerary_id = ${itinerary.id}`
                    );
                    for (let destination of destinations.rows) {
                        itinerary.destinations.push(destination);
                    }
                    return itinerary;
                })
            );
            res.json(itineraryWithEvents);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Get user bookmarked itineraries
itineraryRoutes.get('/bookmark', async (req, res) => {
    try {
        const itineraryList = await client.query(
            `SELECT itinerary.id, itinerary.name, itinerary.days, itinerary.owner_id, users.profile_name, users.photo, bookmarked.count AS bookmark
            FROM itinerary 
            INNER JOIN users ON itinerary.owner_id = users.id
            LEFT OUTER JOIN 
            (SELECT id, itinerary_id, COUNT(*) FROM bookmark WHERE user_id = $1 GROUP BY id, itinerary_id) AS bookmarked
            ON itinerary.id = bookmarked.itinerary_id
            WHERE bookmarked.count IS NOT NULL
            ORDER BY bookmarked.id DESC`,
            [req.session['user']?.id]
        );
        if (!itineraryList.rows[0]) {
            res.status(400).json({ message: 'Itinerary not found' });
        } else {
            const itineraryWithEvents = await Promise.all(
                itineraryList.rows.map(async (itinerary) => {
                    // add bookmark count to result
                    const bookmarkCount = await client.query(
                        `SELECT COUNT(*) FROM bookmark WHERE itinerary_id = ${itinerary.id}`
                    );
                    itinerary.bookmarkCount = bookmarkCount.rows[0].count;
                    // add itinerary events to an array
                    itinerary.events = [];
                    const itineraryEvents = await client.query(
                        `SELECT itinerary_event.name FROM itinerary_event
                    WHERE type = 'main' AND itinerary_id = ${itinerary.id}`
                    );
                    for (let event of itineraryEvents.rows) {
                        itinerary.events.push(event.name);
                    }
                    // add destinations to an array
                    itinerary.destinations = [];
                    const destinations = await client.query(
                        `SELECT destination_group.city, destination_group.country 
                        FROM destination_group
                        INNER JOIN destination
                        ON destination_group.id = destination.destination_group_id
                        WHERE itinerary_id = ${itinerary.id}`
                    );
                    for (let destination of destinations.rows) {
                        itinerary.destinations.push(destination);
                    }
                    return itinerary;
                })
            );

            res.json(itineraryWithEvents);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Get itinerary data
itineraryRoutes.get('/:id', async (req, res) => {
    try {
        let isOwner = true;

        const itineraryId = req.params.id;
        const result = await client.query(
            'SELECT name, days, start_date, owner_id, public FROM itinerary WHERE id = $1',
            [itineraryId]
        );
        if (!result.rows[0]) {
            res.status(400).json({ message: 'Itinerary not found' });
            return;
        } else if (req.session?.['user']) {
            const sessionUser = req.session['user'].id;
            if (result.rows[0].owner_id !== sessionUser) {
                isOwner = false;
            }
            res.json({ ...result.rows[0], isOwner: isOwner });
            return;
        }
        res.json(result.rows[0]);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get itinerary for logged in users
itineraryRoutes.get('/', async (req, res) => {
    try {
        let userID;

        if (req.session?.['user'].id) {
            userID = req.session['user'].id;
        }

        const query = 'SELECT * FROM itinerary WHERE owner_id = $1 LIMIT 5';

        const result = await client.query(query, [userID]);

        if (!result.rows[0]) {
            res.status(400).json({ message: 'No itinerary was found' });
            return;
        }

        res.json(result.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Create bookmark
itineraryRoutes.post('/bookmark/:id', async (req, res) => {
    try {
        if (!req.session['user']) {
            res.json({ message: 'Login to proceed' });
            return;
        }
        const created = await client.query(
            `INSERT INTO bookmark (user_id, itinerary_id) SELECT $1, $2
            WHERE NOT EXISTS (SELECT * FROM bookmark WHERE user_id = $1 AND itinerary_id = $2)`,
            [req.session['user'].id, req.params.id]
        );
        console.log(`created ${created.rowCount}`);
        if (created.rowCount) {
            res.json({
                message: 'Bookmark added',
            });
        } else {
            res.status(400).json({ message: 'Already Bookmarked' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Create new itinerary -- added city, country
// Create new itinerary
itineraryRoutes.post('/', async (req, res) => {
    try {
        const newItinerary: Itinerary = req.body;
        const ownerId: number = req.session['user'].id;
        const result = await client.query(
            'INSERT INTO itinerary (name, days, start_date, owner_id, public) VALUES ($1, $2, $3, $4, $5) RETURNING *',
            [
                newItinerary.name,
                newItinerary.days,
                newItinerary.startDate,
                ownerId,
                newItinerary.public,
            ]
        );

        res.json({
            message: 'Itinerary created',
            id: result.rows[0].id,
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Update itinerary
itineraryRoutes.put('/:id', isMember, async (req, res) => {
    try {
        const updateItinerary: Itinerary = req.body;
        const itineraryId = req.params.id;
        // check if start date changed & update itinerary events time
        const currentStartDate = await client.query(
            `SELECT start_date + interval '8 hour' AS start_date FROM itinerary WHERE id = $1`,
            [itineraryId]
        );
        console.log(new Date(updateItinerary.startDate!));
        console.log(currentStartDate.rows[0].start_date);
        const dayDelta =
            (new Date(updateItinerary.startDate!)?.getTime() -
                currentStartDate.rows[0].start_date?.getTime()) /
            (1000 * 60 * 60 * 24);
        if (dayDelta) {
            console.log(dayDelta);
            console.log('hello');
            await client.query(
                `UPDATE itinerary_event SET start_time = start_time + '${dayDelta} day', end_time = end_time + interval '${dayDelta} day' WHERE itinerary_id = $1`,
                [itineraryId]
            );
        }
        // update itinerary entry
        const result: QueryResult<Itinerary> = await client.query(
            'UPDATE itinerary SET name = $1, days = $2, start_date = $3, public = $4, updated_at = $5 WHERE id = $6 RETURNING *',
            [
                updateItinerary.name,
                updateItinerary.days,
                updateItinerary.startDate,
                updateItinerary.public,
                new Date(),
                itineraryId,
            ]
        );
        console.log(result.rows[0]);
        res.json({ message: 'Itinerary updated' });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Delete bookmark
itineraryRoutes.delete('/bookmark/:id', async (req, res) => {
    try {
        if (!req.session['user']) {
            res.json({ message: 'Login to proceed' });
            return;
        }
        const deleted = await client.query(
            'DELETE FROM bookmark WHERE user_id = $1 AND itinerary_id = $2',
            [req.session['user'].id, req.params.id]
        );
        console.log(`delete ${deleted.rowCount}`);
        if (deleted.rowCount) {
            res.json({
                message: 'Bookmark removed',
            });
        } else {
            res.status(400).json({ message: 'Bad Request' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Delete itinerary
itineraryRoutes.delete('/:id', async (req, res) => {
    try {
        const itineraryId = req.params.id;
        const checkItinerary = await client.query(
            'SELECT count(*) from itinerary where id = $1',
            [itineraryId]
        );
        if (checkItinerary.rows[0]['count'] == 0) {
            res.status(400).json({ message: 'Itinerary not found' });
            return;
        } else {
            await client.query('DELETE FROM itinerary where id = $1', [
                itineraryId,
            ]);
            res.json({ message: 'Itinerary deleted' });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

async function isMember(req: Request, res: Response, next: NextFunction) {
    const query = 'SELECT * FROM itinerary_member WHERE itinerary_id = $1';
    const resultMember = await client.query(query, [req.params.id]);
    const members = resultMember.rows;
    const isMember = members.some((member) => {
        return member.user_id === req.session['user'].id;
    });
    if (!isMember) {
        res.status(400).json({
            message: `user is not the member of this itinerary`,
        });
        return;
    }
    next();
}
