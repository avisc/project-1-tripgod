import express, { Request, Response } from 'express';
import { client } from '../main';

export const userRoutes = express.Router();

userRoutes.get('/:id', async (req: Request, res: Response) => {
    try {
        const userID = req.params.id;

        const query = 'SELECT * FROM users WHERE id = $1';
        const result = await client.query(query, [userID]);

        res.json(result.rows[0]);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});
