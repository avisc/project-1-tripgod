/**
 * This is the /itinerary route
 *
 * GET /
 * Fetch owner user profile
 *
 * GET /:id
 * Fetch any user profile
 *
 * PUT /:id
 * Update user setting
 *
 * DELETE /:id
 * Delete user entry
 *
 */

import express from 'express';
import { client } from '../main';
import { ProfileSetting } from '../model';
import multer from 'multer';
import path from 'path';

export const profileRoutes = express.Router();

// Multer setup
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, `../public/upload`));
    },
    filename: function (req, file, cb) {
        cb(null, `${req.session['user'].id}.${file.mimetype.split('/')[1]}`);
    },
});
const upload = multer({ storage });

profileRoutes.get('/', async (req, res) => {
    try {
        const userId = req.session['user']?.id;
        if (!userId) {
            res.json({ message: 'Login to get user information' });
            return;
        }
        const result = await client.query('SELECT * from users WHERE id = $1', [
            userId,
        ]);
        res.json(result.rows[0]);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

profileRoutes.get('/:id', async (req, res) => {
    try {
        const userId = req.params.id;
        const result = await client.query(
            'SELECT id, profile_name, photo, info, interest from users WHERE id = $1',
            [userId]
        );
        if (!result.rows[0]) {
            res.json({ message: 'User not found' });
        } else {
            res.json(result.rows[0]);
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

profileRoutes.put('/', upload.single('profile'), async (req, res) => {
    try {
        const updateSetting: ProfileSetting = req.body;
        const userId = req.session['user'].id;
        await client.query(
            'UPDATE users SET profile_name = $1, photo = COALESCE($2, photo), info = $3, interest = $4, dark_mode = $5, updated_at = now() WHERE id = $6',
            [
                updateSetting.profileName,
                req.file?.filename,
                updateSetting.info,
                JSON.parse(updateSetting.interest),
                updateSetting.darkMode,
                userId,
            ]
        );
        res.json({
            message: 'Profile setting updated',
            id: req.session['user'].id,
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

// Check and Delete User
profileRoutes.delete('/:id', async (req, res) => {
    try {
        const userId = req.params.id;
        const checkUser = await client.query(
            'SELECT count(*) from users where id = $1',
            [userId]
        );

        const localUser = await client.query(
            'SELECT * from local_account where user_id = $1',
            [userId]
        );
        const googleUser = await client.query(
            'SELECT * from google_account where user_id = $1',
            [userId]
        );
        console.log(localUser);

        if (checkUser.rows[0]['count'] == 0) {
            res.status(400).json({ message: 'User not found' });
            return;
        }
        if (localUser) {
            const delLocal = await client.query(
                'DELETE FROM local_account where user_id = $1 RETURNING *',
                [userId]
            );
            await client.query('DELETE FROM users where id = $1', [
                delLocal.rows[0].user_id,
            ]);
            res.json({ message: 'User deleted' });
        }
        if (googleUser) {
            await client.query(
                'DELETE FROM google_account where user_id = $1 RETURNING *',
                [userId]
            );

            res.json({ message: 'User deleted' });
        } else {
            res.status(400).json({ message: 'User not found' });
            return;
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});
