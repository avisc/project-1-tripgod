import express, { Request, Response } from 'express';
import { client } from '../main';

export const destinationRoutes = express.Router();

//Create new destination
destinationRoutes.post('/', async (req: Request, res: Response) => {
    try {
        const destinationArr = req.body.destinationArr;
        const itineraryID = req.body.itinerary_id;
        let finalResult;
        let destinationGroupID;

        //Check if the city country already exist
        const query =
            'SELECT * FROM destination_group WHERE city=$1 AND country=$2';

        for (let destination of destinationArr) {
            console.log(destination);

            const result = await client.query(query, [
                destination.city,
                destination.country,
            ]);

            //Insert new city country pair
            if (!result.rows[0]) {
                const newDestinationGroup = `INSERT INTO destination_group(city, country) VALUES ($1, $2) RETURNING id`;
                const insert = await client.query(newDestinationGroup, [
                    destination.city,
                    destination.country,
                ]);
                destinationGroupID = insert.rows[0].id;
                console.log(
                    `destinationGroupID (new country): ${destinationGroupID}`
                );
            } else {
                destinationGroupID = result.rows[0].id;
                console.log(
                    `destinationGroupID (already in DB): ${destinationGroupID}`
                );
            }

            //Insert into destination of new itinerary and destination group pair
            const newItinerary = `INSERT INTO destination(destination_group_id, itinerary_id) VALUES ($1, $2) RETURNING *`;
            finalResult = await client.query(newItinerary, [
                destinationGroupID,
                itineraryID,
            ]);
        }

        console.log(finalResult?.rows);

        res.json(finalResult);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get all destinations
destinationRoutes.get('/', async (req, res) => {
    try {
        const destQuery = `SELECT city, country FROM destination_group ORDER BY country`;
        const destinationData = await client.query(destQuery);
        res.json(destinationData.rows);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get destinations of an itinerary
destinationRoutes.get('/:itineraryID', async (req, res) => {
    try {
        const itineraryID = req.params.itineraryID;

        const query = 'SELECT * FROM destination WHERE itinerary_id = $1';

        const result = await client.query(query, [itineraryID]);

        console.log(result.rows);

        const destinationArr = [];

        for (let destination of result.rows) {
            const destinationID = destination.destination_group_id;

            const destQuery = `SELECT * FROM destination_group WHERE id = $1`;
            const destinationData = await client.query(destQuery, [
                destinationID,
            ]);
            destinationArr.push(destinationData.rows[0]);
        }

        res.json(destinationArr);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});

destinationRoutes.delete('/:itineraryID', async (req, res) => {
    try {
        const destinationArr = req.body;
        const itineraryID = req.params.itineraryID;

        console.log(destinationArr);

        for (let destination of destinationArr) {
            //Get group id of destination
            const select =
                'SELECT * FROM destination INNER JOIN destination_group ON destination.destination_group_id = destination_group.id WHERE destination.itinerary_id = $1 AND destination_group.city = $2 AND destination_group.country = $3';

            const selectResult = await client.query(select, [
                itineraryID,
                destination.city,
                destination.country,
            ]);

            const desData = selectResult.rows[0];

            const query =
                'DELETE FROM destination WHERE itinerary_id = $1 AND destination_group_id = $2';

            await client.query(query, [
                desData.itinerary_id,
                desData.destination_group_id,
            ]);
        }

        res.json({ message: 'successfully deleted' });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});
