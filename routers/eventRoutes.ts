import express, { Request, Response } from 'express';
import { client } from '../main';

export const eventRoutes = express.Router();

//Create new event and get the id of the event created
eventRoutes.post('/', async (req: Request, res: Response) => {
    try {
        const itinerary_id = req.body.id;
        const title = req.body.name;
        const type = req.body.type;
        const startTime = req.body.start_time;
        const endTime = req.body.end_time;

        const query =
            'INSERT INTO itinerary_event(itinerary_id, name, type, start_time,end_time) VALUES ($1, $2, $3, $4, $5) RETURNING id';
        const eventID = await client.query(query, [
            itinerary_id,
            title,
            type,
            startTime,
            endTime,
        ]);

        console.log(eventID.rows[0].id);

        res.json({ eventID: eventID.rows[0].id });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

eventRoutes.post('/:itineraryID', async (req: Request, res: Response) => {
    try {
        let order = 1;
        let query = 'INSERT INTO itinerary_event(';
        let queryEnd = ' VALUES (';
        let queryArr = [];

        for (let key in req.body) {
            if (key === 'id') {
                continue;
            }
            query += `${key}, `;
            queryEnd += `$${order}, `;
            order++;
            queryArr.push(req.body[key]);
        }

        query =
            query.slice(0, query.length - 2) +
            ') ' +
            queryEnd.slice(0, queryEnd.length - 2) +
            ') ' +
            ' RETURNING *';

        const result = await client.query(query, queryArr);

        console.log(result);

        res.json(result.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Update itinerary event right after creation
eventRoutes.put('/:id', async (req, res) => {
    try {
        const id = req.params.id;
        let order = 1;
        let query = 'UPDATE itinerary_event SET ';
        let queryArr = [];

        for (let key in req.body) {
            query += `${key} = $${order}, `;
            order++;
            queryArr.push(req.body[key]);
        }

        query = query.slice(0, query.length - 2) + ` WHERE id = $${order}`;
        queryArr.push(id);

        await client.query(query, queryArr);

        res.json({ message: 'Update successful.' });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Delete the freshly created event when X is clicked
eventRoutes.delete('/', async (req, res) => {
    try {
        const id = req.body.id;

        const query = 'DELETE FROM itinerary_event WHERE id = $1';

        await client.query(query, [id]);

        res.json({ message: 'Event deleted' });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get information of an event clicked
eventRoutes.get('/:id', async (req, res) => {
    try {
        const id = req.params.id;

        const query = 'SELECT * FROM itinerary_event WHERE id = $1';

        const result = await client.query(query, [id]);

        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});

//Get all the events in an itinerary
eventRoutes.get('/all-events/:itineraryID', async (req, res) => {
    try {
        const id = req.params.itineraryID;

        const query = 'SELECT * FROM itinerary_event WHERE itinerary_id = $1';

        const result = await client.query(query, [id]);

        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
});
