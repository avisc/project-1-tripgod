import express from 'express';
import { client } from '../main';

export const adminRoutes = express.Router();

// Check Admin
adminRoutes.get('/', async (req, res) => {
    try {
        const userId = req.session['user']?.id;
        const result = await client.query(
            'SELECT * from admin WHERE user_id = $1',
            [userId],
        );
        console.log(userId)
        console.log(result.rows[0])

        if (!userId) {
            res.json('Login to get user information');
            return;
        }
        if (result.rows[0]) {
            res.json(result.rows[0]);
        } else {
            res.json('Welcome Back')
        }

    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: 'Internal server error' });
    }
});