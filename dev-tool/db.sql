-- Create database
CREATE DATABASE c13p1g1_travel;

\c c13p1g1_travel;


CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    profile_name VARCHAR(255) NOT NULL,
    photo VARCHAR(255),
    info VARCHAR(255),
    interest VARCHAR(255) [] NOT NULL DEFAULT '{}',
    dark_mode boolean NOT NULL DEFAULT false,
    created_at timestamp NOT NULL DEFAULT NOW(),
    updated_at timestamp NOT NULL DEFAULT NOW()
);


CREATE TABLE local_account(
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    username VARCHAR(255) NOT NULL,
    hashed_password VARCHAR(255) NOT NULL
);


CREATE TABLE google_account(
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    email VARCHAR(255) NOT NULL
);


CREATE TABLE itinerary(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    days INTEGER NOT NULL,
    start_date DATE,
    owner_id INTEGER NOT NULL,
    public boolean NOT NULL DEFAULT FALSE,
    FOREIGN KEY (owner_id) REFERENCES users(id),
    created_at timestamp NOT NULL DEFAULT NOW(),
    updated_at timestamp NOT NULL DEFAULT NOW()
);


CREATE TABLE destination_group(
    id SERIAL PRIMARY KEY,
    city VARCHAR(255),
    country VARCHAR(255) NOT NULL
);


CREATE TABLE destination(
    id SERIAL PRIMARY KEY,
    destination_group_id INTEGER NOT NULL,
    FOREIGN KEY (destination_group_id) REFERENCES destination_group(id) ,
    itinerary_id INTEGER NOT NULL,
    FOREIGN KEY (itinerary_id) REFERENCES itinerary(id) ON DELETE CASCADE
);


CREATE TABLE itinerary_event(
    id SERIAL PRIMARY KEY,
    itinerary_id INTEGER NOT NULL,
    FOREIGN KEY (itinerary_id) REFERENCES itinerary(id) ON DELETE CASCADE,
    name VARCHAR(255) NOT NULL,
    TYPE VARCHAR(255) NOT NULL,
    start_time timestamp NOT NULL,
    end_time timestamp NOT NULL,
    location VARCHAR(255),
    remark TEXT
);
    

CREATE TABLE itinerary_member(
    id SERIAL PRIMARY KEY,
    itinerary_id INTEGER NOT NULL,
    FOREIGN KEY (itinerary_id) REFERENCES itinerary(id) ON DELETE CASCADE,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);


CREATE TABLE bookmark(
    id SERIAL PRIMARY KEY,
    itinerary_id INTEGER NOT NULL,
    FOREIGN KEY (itinerary_id) REFERENCES itinerary(id) ON DELETE CASCADE,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

-- -- Test data for local account--
--plain string password = 1234--
INSERT INTO users(profile_name, info)
VALUES ('test', 'Good Morning!!!');

INSERT INTO local_account(user_id, username, hashed_password)
VALUES (
        (
            SELECT id
            FROM users
            WHERE profile_name = 'test'
        ),
        'test@test.com',
        '$2y$10$9etNVKHnGw1I5HUL5zzkq.9V6eE4Xj913pFgHHDJUEpReqTyLnJXa'
    );

--plain string password = 1111--
INSERT INTO users(profile_name, info, photo)
VALUES ('test 2', 'よろしくね！', '2.jpg');

INSERT INTO local_account(user_id, username, hashed_password)
VALUES (
        (
            SELECT id
            FROM users
            WHERE profile_name = 'test 2'
        ),
        'test2',
        '$2a$10$7ZE1H7XjAZcj3EyS.o1YhOToScqP9hIuIHeJL9QwLubsxoWz9jfRK'
    );

-- Test data for google account--
INSERT INTO users(profile_name)
VALUES ('google test');

INSERT INTO google_account(user_id, email)
VALUES (
        (
            SELECT id
            FROM users
            WHERE profile_name = 'google test'
        ),
        'acounttesting900@gmail.com'
    );

-- Dummy data for itinerary--
INSERT INTO itinerary (name, days, start_date, owner_id, public)
VALUES ('dummy itinerary 1', 5, NOW(), 1, false),
    (
        'dummy itinerary 2',
        3,
        NOW() + INTERVAL '1 day',
        1,
        true
    ),
    (
        'dummy itinerary 3',
        9,
        NOW() + INTERVAL '100 day',
        2,
        true
    ),
    (
        'dummy itinerary 4',
        1,
        NOW() + INTERVAL '365 day',
        2,
        true
    );

-- Dummy data for destination group--
INSERT INTO destination_group (city, country)
VALUES ('City 1', 'Country 1'),
    ('City 2', 'Country 2'),
    ('City 3', 'Country 3');

-- Dummy data for destination--
INSERT INTO destination (destination_group_id, itinerary_id)
VALUES (1, 1),
    (2, 2),
    (3, 3);

-- Dummy data for itinerary event--
INSERT INTO itinerary_event (
        itinerary_id,
        name,
        TYPE,
        start_time,
        end_time
    )
VALUES (
        1,
        'event 1',
        'main',
        NOW(),
        NOW() + INTERVAL '2 hour'
    ),
    (
        1,
        'transport 1',
        'transport',
        NOW() + INTERVAL '2 hour',
        NOW() + INTERVAL '3 hour'
    ),
    (
        2,
        'event 2',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '20 hours',
        NOW() + INTERVAL '2 day' + INTERVAL '20 hours'
    ),
    (
        3,
        'event 3',
        'main',
        NOW() + INTERVAL '108 day',
        NOW() + INTERVAL '108 day' + INTERVAL '8 hours'
    ),
    (
        3,
        'event 4',
        'main',
        NOW() + INTERVAL '102 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '102 day' + INTERVAL '4 hours'
    ),
    (
        4,
        'event 5',
        'main',
        NOW() + INTERVAL '365 day',
        NOW() + INTERVAL '365 day'  + INTERVAL '3 hour'
    );

-- Dummy data for itinerary member--
INSERT INTO itinerary_member (itinerary_id, user_id)
VALUES (1, 1),
    (2, 1),
    (2, 2),
    (3, 2),
    (4, 1),
    (4, 2),
    (5, 1);


-- Dummy data for bookmark--
INSERT INTO bookmark (itinerary_id, user_id)
VALUES (2, 2),
    (4, 1);

-- Create admin table --
CREATE TABLE admin (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Admin Right --
INSERT INTO admin (user_id) VALUES (1);

-- Dummy data for itinerary : Australia Trip --
INSERT INTO itinerary (name, days, start_date, owner_id, public)
VALUES ('澳洲9日之旅', 9, NOW(), 1, true);

-- Dummy data for itinerary event : Australia Trip --
INSERT INTO itinerary_event (
        itinerary_id,
        name,
        TYPE,
        start_time,
        end_time
    )VALUES(
        5,
        'Flight',
        'transport',
        NOW(),
        NOW() + INTERVAL '9 hour'
    ),
    (
        5,
        'Sydney Airport Train',
        'transport',
        NOW() + INTERVAL '9 hour',
        NOW() + INTERVAL '10 hour'
    ),
    (
        5,
        'Darling Harbour',
        'main',
        NOW() + INTERVAL '10 hour',
        NOW() + INTERVAL '12 hour'
    ),
    (
        5,
        'Pancake On The Rocks',
        'main',
        NOW() + INTERVAL '12 hour',
        NOW() + INTERVAL '14 hour'
    ),
    (
        5,
        'Light Rail – Fish Market Station',
        'transport',
        NOW() + INTERVAL '1 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '2 hour'
    ),
    (
        5,
        'Sydney Fish Market',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '2 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '4 hour'
    ),
    (
        5,
        'Harry’s cafe de Wheels',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '4 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '5 hour'
    ),
    (
        5,
        'Town Hall Station',
        'transport',
        NOW() + INTERVAL '1 day' + INTERVAL '5 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '6 hour'
    ),
    (
        5,
        'ST Andrew’s Cathedral',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '6 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '7 hour'
    ),
    (
        5,
        'Circular Quay',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '7 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '9 hour'
    ),
    (
        5,
        'Sydney Opera House',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '9 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '10 hour'
    ),
    (
        5,
        'Nandos',
        'main',
        NOW() + INTERVAL '1 day' + INTERVAL '10 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '12 hour'
    ),
    (
        5,
        'Light Rail',
        'transport',
        NOW() + INTERVAL '1 day' + INTERVAL '12 hour',
        NOW() + INTERVAL '1 day' + INTERVAL '13 hour'
    ),
    (
        5,
        'Bus',
        'transport',
        NOW() + INTERVAL '2 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '2 day' + INTERVAL '3 hour'
    ),
    (
        5,
        'Blue Mountains National Park',
        'main',
        NOW() + INTERVAL '2 day' + INTERVAL '3 hour',
        NOW() + INTERVAL '2 day' + INTERVAL '5 hour'
    ),
    (
        5,
        'Bus',
        'transport',
        NOW() + INTERVAL '2 day' + INTERVAL '5 hour',
        NOW() + INTERVAL '2 day' + INTERVAL '7 hour'
    ),
    (
        5,
        'Jenolan Caves',
        'main',
        NOW() + INTERVAL '2 day' + INTERVAL '7 hour',
        NOW() + INTERVAL '2 day' + INTERVAL '9 hour'
    ),
    (
        5,
        'Bus',
        'transport',
        NOW() + INTERVAL '2 day' + INTERVAL '9 hour',
        NOW() + INTERVAL '2 day' + INTERVAL '11 hour'
    ),
    (
        5,
        'City Circle Tram',
        'transport',
        NOW() + INTERVAL '3 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '2 hour'
    ),
    (
        5,
        'The Hardware Societe',
        'main',
        NOW() + INTERVAL '3 day' + INTERVAL '2 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '3 hour'
    ),
    (
        5,
        'Tram 35',
        'transport',
        NOW() + INTERVAL '3 day' + INTERVAL '3 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '4 hour'
    ),
    (
        5,
        'Melbourne Town Hall',
        'main',
        NOW() + INTERVAL '3 day' + INTERVAL '4 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '5 hour'
    ),
    (
        5,
        'Hosier Lane',
        'main',
        NOW() + INTERVAL '3 day' + INTERVAL '5 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '6 hour'
    ),
    (
        5,
        'Melbourne Central Station',
        'transport',
        NOW() + INTERVAL '3 day' + INTERVAL '6 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '7 hour'
    ),
    (
        5,
        'Ludlow Bar & Dining Room',
        'main',
        NOW() + INTERVAL '3 day' + INTERVAL '7 hour',
        NOW() + INTERVAL '3 day' + INTERVAL '9 hour'
    ),
    (
        5,
        'Queen Victoria Market',
        'main',
        NOW() + INTERVAL '4 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '4 day' + INTERVAL '3 hour'
    ),
    (
        5,
        'Day Tour Bus',
        'transport',
        NOW() + INTERVAL '4 day' + INTERVAL '3 hour',
        NOW() + INTERVAL '4 day' + INTERVAL '5 hour'
    ),
    (
        5,
        'Brighton Beach',
        'main',
        NOW() + INTERVAL '4 day' + INTERVAL '5 hour',
        NOW() + INTERVAL '4 day' + INTERVAL '6 hour'
    ),
    (
        5,
        'Phillip Island',
        'main',
        NOW() + INTERVAL '4 day' + INTERVAL '6 hour',
        NOW() + INTERVAL '4 day' + INTERVAL '10 hour'
    ),
    (
        5,
        'Day Tour Bus',
        'transport',
        NOW() + INTERVAL '4 day' + INTERVAL '10 hour',
        NOW() + INTERVAL '4 day' + INTERVAL '11 hour'
    ),
    (
        5,
        'Pancake Parlour',
        'main',
        NOW() + INTERVAL '4 day' + INTERVAL '11 hour',
        NOW() + INTERVAL '4 day' + INTERVAL '12 hour'
    ),
    (
        5,
        'Day Tour Bus',
        'transport',
        NOW() + INTERVAL '5 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '5 day' + INTERVAL '3 hour'
    ),
    (
        5,
        'Great Ocean Road',
        'main',
        NOW() + INTERVAL '5 day' + INTERVAL '3 hour',
        NOW() + INTERVAL '5 day' + INTERVAL '4 hour'
    ),
    (
        5,
        'The Twelve Apostles',
        'main',
        NOW() + INTERVAL '5 day' + INTERVAL '4 hour',
        NOW() + INTERVAL '5 day' + INTERVAL '6 hour'
    ),
    (
        5,
        'Day Tour Bus',
        'transport',
        NOW() + INTERVAL '5 day' + INTERVAL '6 hour',
        NOW() + INTERVAL '5 day' + INTERVAL '8 hour'
    ),
    (
        5,
        'Pho Dzung Tan Dinh',
        'main',
        NOW() + INTERVAL '5 day' + INTERVAL '8 hour',
        NOW() + INTERVAL '5 day' + INTERVAL '9 hour'
    ),
    (
        5,
        'Tram',
        'transport',
        NOW() + INTERVAL '6 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '6 day' + INTERVAL '3 hour'
    ),
    (
        5,
        'Luna Park',
        'main',
        NOW() + INTERVAL '6 day' + INTERVAL '3 hour',
        NOW() + INTERVAL '6 day' + INTERVAL '5 hour'
    ),
    (
        5,
        'St. Kilda Beach',
        'main',
        NOW() + INTERVAL '6 day' + INTERVAL '5 hour',
        NOW() + INTERVAL '6 day' + INTERVAL '8 hour'
    ),
    (
        5,
        'Tram',
        'transport',
        NOW() + INTERVAL '6 day' + INTERVAL '8 hour',
        NOW() + INTERVAL '6 day' + INTERVAL '9 hour'
    ),
    (
        5,
        'Queen Victoria Market (Night Market)',
        'main',
        NOW() + INTERVAL '6 day' + INTERVAL '9 hour',
        NOW() + INTERVAL '6 day' + INTERVAL '11 hour'
    ),
    (
        5,
        'Day Tour Bus',
        'transport',
        NOW() + INTERVAL '7 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '7 day' + INTERVAL '3 hour'
    ),
    (
        5,
        'Moonlit Sanctuary Wildlife Conservation Park',
        'main',
        NOW() + INTERVAL '7 day' + INTERVAL '3 hour',
        NOW() + INTERVAL '7 day' + INTERVAL '7 hour'
    ),
    (
        5,
        'Day Tour Bus',
        'transport',
        NOW() + INTERVAL '7 day' + INTERVAL '7 hour',
        NOW() + INTERVAL '7 day' + INTERVAL '9 hour'
    ),
    (
        5,
        'Nandos',
        'main',
        NOW() + INTERVAL '7 day' + INTERVAL '9 hour',
        NOW() + INTERVAL '7 day' + INTERVAL '10 hour'
    ),
    (
        5,
        'Flight',
        'transport',
        NOW() + INTERVAL '8 day' + INTERVAL '1 hour',
        NOW() + INTERVAL '8 day' + INTERVAL '10 hour'
    );