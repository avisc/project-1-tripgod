window.onload = () => {
    registerRequest();
};

function registerRequest() {
    const registerForm = document.querySelector('#register');

    registerForm.addEventListener('submit', async (event) => {
        try {
            event.preventDefault();

            const username = document.querySelector('#username').value;
            const email = document.querySelector('#email').value;
            let password = document.querySelector('#password').value;
            let retypePassword = document.querySelector('#retype-password')
                .value;

            // check if BOTH username and password are entered
            if (!email || !username || !password || !retypePassword) {
                alert('Please enter all the field!');
                return;
            }

            // Roy: DOM elements already assigned variables?
            if (password !== retypePassword) {
                alert(`passwords doesn't match!`);
                document.querySelector('#password').value = '';
                document.querySelector('#retype-password').value = '';
                return;
            }

            const formData = {
                name: username,
                email: email,
                password: password,
            };

            const res = await fetch('/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                body: JSON.stringify(formData),
            });

            const response = await res.json();

            //If that email has been registered
            // Roy: DOM elements already assigned variables?
            if (res.status === 400) {
                alert(response.message);
                document.querySelector('#password').value = '';
                document.querySelector('#retype-password').value = '';
            }

            if (res.status === 200) {
                window.location = '/index.html';
            }
        } catch (err) {
            console.error(err);
        }
    });
}
