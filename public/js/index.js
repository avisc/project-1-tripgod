window.onload = () => {
    displayUserInfo();
    loadDestination();
    loadItinerary('latest');
    adminButton();
    loginRequest();
};

let data = {
    myId: 0,
    profileName: '',
    myPhoto: '',
    days: '',
    ownerId: '',
    darkMode: '',
    destinationList: [],
    itineraryList: [],
    admin: false,
    loginOverlay: false,
    innerWidth: window.innerWidth,
};

window.addEventListener('resize', () => {
    data.innerWidth = window.innerWidth;
});

const app = new Vue({
    el: '#vue',
    data: data,
    computed: {
        loggedOut: function () {
            if (this.myId === 0) {
                return false;
            }
            if (this.myId) {
                return false;
            } else {
                return true;
            }
        },
        loggedIn: function () {
            if (this.myId) {
                return true;
            } else {
                return false;
            }
        },
        windowWidth: function () {
            return this.innerWidth > 576;
        },
    },
    methods: {
        itineraryView: function (itinerary) {
            window.location = `itinerary_view.html?id=${itinerary.id}`;
        },
        toggleLogin: function () {
            this.loginOverlay = !this.loginOverlay;
            loginForm.reset();
        },
        logout: async function () {
            const res = await fetch('/login', {
                method: 'DELETE',
            });
            if (res.status === 200) {
                (data.myId = ''),
                    (data.profileName = ''),
                    (data.myPhoto = ''),
                    (data.admin = false);
            } else {
                const err = await res.json();
                alert(err.message);
            }
        },
        addBookmark: async function (itinerary, event) {
            if (this.loggedIn) {
                itinerary.totalbookmark = Number(itinerary.totalbookmark) + 1;
                itinerary.bookmark = 1;
            }
            const target = event.currentTarget;
            const res = await fetch(`/itinerary/bookmark/${itinerary.id}`, {
                method: 'POST',
            });
            const response = await res.json();
            console.log(response);
            if (response.message === 'Login to proceed') {
                this.toggleLogin();
                return;
            }
        },
        removeBookmark: async function (itinerary, event) {
            if (this.loggedIn) {
                itinerary.totalbookmark = Number(itinerary.totalbookmark) - 1;
            }
            itinerary.bookmark = 0;
            const target = event.currentTarget;
            const res = await fetch(`/itinerary/bookmark/${itinerary.id}`, {
                method: 'DELETE',
            });
            const response = await res.json();
            if (response.message === 'Login to proceed') {
                this.toggleLogin();
                return;
            }
        },
        callLoadItinerary: function (sortBy) {
            loadItinerary(sortBy);
        },
    },
});

async function displayUserInfo() {
    const res = await fetch('/profile');
    userInfo = await res.json();
    //console.log(userInfo);
    data.myId = userInfo.id;
    data.profileName = userInfo.profile_name;
    data.darkMode = userInfo.dark_mode;
    if (!userInfo.photo) {
        data.myPhoto = `/img/dummy_profilepic.jpg`;
    } else {
        data.myPhoto = `/upload/${userInfo.photo}`;
    }
    // Dark Mode
    if (userInfo.dark_mode === true) {
        document.body.classList.toggle('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

async function loadItinerary(sortBy) {
    const res = await fetch(`/itinerary/all?sort=${sortBy}`);
    itineraryList = await res.json();
    data.itineraryList = [...itineraryList];
    //console.log(itineraryList);
}

async function adminButton() {
    const res = await fetch('/admin');
    adminInfo = await res.json();
    if (adminInfo.id) {
        data.admin = true;
    } else {
        data.admin = false;
    }
}

document
    .querySelector('#my-itinerary-btn')
    .addEventListener('click', async () => {
        try {
            const res = await fetch('/itinerary');

            const result = await res.json();

            if (res.status === 400) {
                window.location = '/itinerary.html';
                return;
            }

            window.location = `/itinerary_edit.html?id=${result[0].id}`;
        } catch (err) {
            console.error(err);
        }
    });

const loginForm = document.querySelector('#user-login');
function loginRequest() {
    //When login form is submitted
    loginForm.addEventListener('submit', async (event) => {
        //console.log('here');
        try {
            event.preventDefault();
            const form = event.target;

            const username = document.querySelector('#username').value; //form.username.value
            const password = document.querySelector('#password').value;
            // check if BOTH username and password are entered
            if (!username || !password) {
                alert('Please enter username/password');
                loginForm.reset();
                return;
            }
            //loginForm.reset();

            const formData = { username: username, password: password };

            const res = await fetch('/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                body: JSON.stringify(formData),
            });

            const response = await res.json();

            //If no user was found
            if (res.status === 400) {
                alert(response.message);
                return;
            }

            if (res.status === 200) {
                //console.log(response);
                data.loginOverlay = false;
                await displayUserInfo();
                await loadItinerary('latest');
                await adminButton();
            }

            // if(res.status === 200 && response.users === "??"){
            // }else{ }
            loginForm.reset();
        } catch (err) {
            console.error(err);
            alert(err.message);
        }
    });
}

async function loadDestination() {
    const res = await fetch('/destination');
    const destinations = await res.json();
    data.destinationList = [...destinations];
}

////////////////////////////////// Timeline
(function () {
    // VARIABLES
    const timeline = document.querySelector('.timeline ol'),
        elH = document.querySelectorAll('.timeline li > div'),
        firstItem = document.querySelector('.timeline li:first-child'),
        lastItem = document.querySelector('.timeline li:last-child'),
        xScrolling = 280,
        disabledClass = 'disabled';

    // START
    window.addEventListener('load', init);

    function init() {
        setEqualHeights(elH);
    }

    // SET EQUAL HEIGHTS
    function setEqualHeights(el) {
        let counter = 0;
        for (let i = 0; i < el.length; i++) {
            const singleHeight = el[i].offsetHeight;

            if (counter < singleHeight) {
                counter = singleHeight;
            }
        }

        for (let i = 0; i < el.length; i++) {
            el[i].style.height = `${counter}px`;
        }
    }

    // CHECK IF AN ELEMENT IS IN VIEWPORT
    // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
    function isElementInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <=
                (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <=
                (window.innerWidth || document.documentElement.clientWidth)
        );
    }
})();
