window.onload = async () => {
    await loadProfile();
    if (!data.userId) {
        return;
    }
    await Promise.all([loadItinerary(), loadBookmark()]);
    data.ready = 1;
};

const params = new URLSearchParams(window.location.search);
const userId = params.get('id');

let data = {
    userId: '',
    profileName: '',
    info: '',
    interests: [],
    darkMode: '',
    itineraries: [],
    bookmarks: [],
    myPhoto: '',
    ready: 0,
    innerWidth: window.innerWidth,
};

window.addEventListener('resize', () => {
    data.innerWidth = window.innerWidth;
});

// Vue app
const app = new Vue({
    el: '#vue',
    data: data,
    computed: {
        isSelf: function () {
            if (userId) {
                return false;
            } else {
                return true;
            }
        },
        collapse: function () {
            return this.innerWidth < 576;
        },
    },
    methods: {
        editItinerary: function (itinerary) {
            window.location = `/itinerary_edit.html?id=${itinerary.id}`;
        },
        itineraryView: function (itinerary) {
            window.location = `/itinerary_view.html?id=${itinerary.id}`;
        },
        addBookmark: async function (itinerary, event) {
            const target = event.currentTarget;
            const res = await fetch(`/itinerary/bookmark/${itinerary.id}`, {
                method: 'POST',
            });
            const response = await res.json();
            if (response.message === 'Login to proceed') {
                window.location = '/login.html';
                return;
            }
            itinerary.bookmarkCount = Number(itinerary.bookmarkCount) + 1;
            itinerary.bookmark = 1;
        },
        removeBookmark: async function (itinerary, event) {
            const target = event.currentTarget;
            const res = await fetch(`/itinerary/bookmark/${itinerary.id}`, {
                method: 'DELETE',
            });
            const response = await res.json();
            if (response.message === 'Login to proceed') {
                window.location = '/login.html';
                return;
            }
            itinerary.bookmarkCount = Number(itinerary.bookmarkCount) - 1;
            itinerary.bookmark = 0;
        },
    },
});

async function loadProfile() {
    const res = await fetch(`/profile/${userId ?? ''}`);
    const result = await res.json();
    if (
        result.message === 'Login to get user information' ||
        result.message === 'User not found'
    ) {
        window.location = '/';
        return;
    }
    data.userId = result.id;
    data.profileName = result.profile_name;
    data.info = result.info;
    data.darkMode = result.dark_mode;
    for (let interest of result.interest) {
        if (interest === 'SEA') {
            data.interests.push('South East Asia');
        } else if (interest === 'EU') {
            data.interests.push('Europe');
        } else if (interest === 'CUS') {
            data.interests.push('Canada-United States');
        } else if (interest === 'LT') {
            data.interests.push('Local Tour');
        }
    }
    if (!result.photo) {
        data.myPhoto = `/img/dummy_profilepic.jpg`;
    } else {
        data.myPhoto = `/upload/${result.photo}`;
    }
    // Dark Mode
    if (result.dark_mode === true) {
        document.body.classList.toggle('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

async function loadItinerary() {
    let fetchTarget;
    if (userId) {
        // itinerary details
        fetchTarget = `/itinerary/user/${userId}/all`;
    } else {
        // itinerary array
        fetchTarget = `/itinerary/user/0`;
    }
    const res = await fetch(fetchTarget);
    const result = await res.json();
    if (res.status === 200) {
        data.itineraries = [...result];
    }
}

async function loadBookmark() {
    if (userId) {
        return;
    } else {
        const res = await fetch('/itinerary/bookmark');
        const result = await res.json();
        //console.log(result);
        if (res.status === 200) {
            data.bookmarks = [...result];
        }
    }
}
