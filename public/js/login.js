window.onload = () => {
    loginRequest();
};

function loginRequest() {
    const loginForm = document.querySelector('#user-login');

    //When login form is submitted
    loginForm.addEventListener('submit', async (event) => {
        //console.log('here');
        try {
            event.preventDefault();

            const username = document.querySelector('#username').value;
            const password = document.querySelector('#password').value;

            // check if BOTH username and password are entered
            if (!username || !password) {
                alert('Please enter username/password');
                return;
            }

            const formData = { username: username, password: password };

            const res = await fetch('/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
                body: JSON.stringify(formData),
            });

            const response = await res.json();

            //If no user was found
            if (res.status === 400) {
                alert(response.message);
            }

            if (res.status === 200) {
                console.log(response);
                window.location = '/index.html'; //id
            }
        } catch (err) {
            console.error(err);
            alert(err.message);
        }
    });
}
