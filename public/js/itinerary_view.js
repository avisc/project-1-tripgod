const TRANSPORT = require('../constants');
const params = new URLSearchParams(window.location.search);
const itineraryID = params.get('id');
let itineraryData;
const itineraries = [];
document.querySelector('.clone').style.display = 'initial';

window.onload = async () => {
    await changeDestinationTitle(itineraryID);
};
// Fetch user itinerary list
async function fetchItineraryList() {
    try {
        const data = await fetch(`/itinerary/user/${itineraryData.ownerId}`);
        results = await data.json();
        //console.log(results);
        for (let result of results) {
            itineraries.push(result);
        }
        return;
    } catch (err) {
        console.log(err);
    }
}

// Vue app
const vue = new Vue({
    el: '#vue',
    data: {
        itineraries: itineraries,
        owner: '',
    },
    methods: {
        hello: function (str) {
            console.log(str);
        },
        loadItinerary: async function (id) {
            try {
                document.querySelector('#calendar').innerHTML = '';
                day = 1;
                itineraryData = await getItineraryData(id);

                calendarEl = document.getElementById('calendar');
                calendar = new FullCalendar.Calendar(calendarEl, setting);
                //Render all Events
                await getAllEvents(id, calendar);

                console.log(calendar.getEvents());
                changeDestinationTitle(id);

                calendar.render();
            } catch (err) {
                console.error(err);
            }
        },
    },
});

//DOM element
//Popup
const popup = document.querySelector('#popup');
const popupForm = document.querySelector('#popup-form');
const closePopupBtn = document.querySelector('.close-popup');

//View
const view = document.querySelector('#view');
const closeViewBtn = document.querySelectorAll('.close');
const deleteEventBtn = document.querySelector('.delete-event');

//Edit Page
const editEventBtn = document.querySelector('.edit-event');
const editEventPage = document.querySelector('#edit-event');
const editForm = document.querySelector('#edit-form');

//Store selected event ID
let selectedEventID;
//Store clicked event ID
let clickedEventID;
//For display day in content header
let day = 1;

let setting = {
    selectable: true,
    height: '100%',
    //Turn of toolbar
    headerToolbar: false,
    initialView: 'timeGrid',
    //Duration and visibleRange will only take one
    //So it is better to use visibleRange with the duration calculated
    visibleRange: function () {
        let startDate = '2021-01-01';
        itineraryData.duration--;

        if (itineraryData.startDate !== null) {
            startDate = itineraryData.startDate;
            itineraryData.duration++;
        }

        endDate = new Date(startDate);
        endDate.setDate(new Date(startDate).getDate() + itineraryData.duration);

        return { start: startDate, end: endDate };
    },
    //Control header content
    dayHeaderContent: (args) => {
        if (itineraryData.startDate === null) {
            args.text = `Day ${day}`;
            day++;
        } else {
            return;
        }
    },
    eventTimeFormat: {
        hour: 'numeric',
        minute: '2-digit',
        meridiem: true,
    },

    //When event is clicked - view Detail
    eventClick: async function (info) {
        //console.log('click');
        const title = document.querySelector('#view-title');
        const address = document.querySelector('#view-address');
        const remark = document.querySelector('#view-remarks');
        const startTime = document.querySelector('#view-start-time');
        const endTime = document.querySelector('#view-end-time');
        const from = document.querySelector('#view-from');

        const eventID = info.event.id;
        clickedEventID = info.event.id;

        const res = await fetch(`/event/${eventID}`);
        const data = await res.json();
        //if(data.length > 0)
        const eventData = data.rows[0];

        //console.log(eventData);

        title.innerHTML = eventData.name;
        //Only show address and remarks if not equal to ""
        if (eventData.location === '') {
            address.parentElement.style.display = 'none';
            from.parentElement.style.display = 'none';
        }

        if (eventData.type === TRANSPORT /* TRANSPORT */) {
            //console.log(JSON.parse(eventData.location));
            if (
                JSON.parse(eventData.location).from === '' ||
                JSON.parse(eventData.location).to === ''
            ) {
                address.parentElement.style.display = 'none';
                from.parentElement.style.display = 'none';
            } else {
                address.parentElement.style.display = 'none';
                from.parentElement.style.display = 'initial';
            }
            const location = JSON.parse(eventData.location);
            from.innerHTML = location.from;
            document.querySelector('#view-to').innerHTML = location.to;
        } else {
            if (eventData.location === '') {
                address.parentElement.style.display = 'none';
            } else {
                address.parentElement.style.display = 'initial';
            }

            address.innerHTML = eventData.location;
        }

        if (!eventData.remark) {
            remark.parentElement.style.display = 'none';
        } else {
            remark.parentElement.style.display = 'initial';
            remark.innerHTML = eventData.remark;
        }

        const eventStartTime = dateObjectToTime(new Date(eventData.start_time));
        const eventEndTime = dateObjectToTime(new Date(eventData.end_time));

        startTime.innerHTML = eventStartTime;
        endTime.innerHTML = eventEndTime;

        view.classList.toggle('view-hidden');
        view.classList.toggle('view-show');

        //Set view position next to event
        const domRect = info.jsEvent.target.getBoundingClientRect();
        view.style.left = domRect.x + 'px';
        view.style.top = domRect.y + 'px';
    },
};

let calendarEl;
let calendar;

//Render calendar
document.addEventListener('DOMContentLoaded', async function () {
    itineraryData = await getItineraryData(itineraryID);
    fetchItineraryList(); // for left hand side itinerary list
    calendarEl = document.getElementById('calendar');
    calendar = new FullCalendar.Calendar(calendarEl, setting);
    calendar.render();

    //Render all Events
    getAllEvents(itineraryData.id, calendar);
});

//Close view
for (let closeBtn of closeViewBtn) {
    closeBtn.addEventListener('click', (e) => {
        e.preventDefault();

        if (e.target.parentElement.id === 'edit-event') {
            editEventPage.classList.toggle('edit-hidden');
            editEventPage.classList.toggle('edit-show');
        }

        view.classList.toggle('view-hidden');
        view.classList.toggle('view-show');
    });
}

//Custom time and day functions
function myGetTime(dateStr) {
    let date = moment(dateStr);
    const dateArr = dateStr.split('T');
    return dateArr[1].slice(0, 5); //date.format("HH:mm")
}
function myGetDate(dateStr) {
    const dateArr = dateStr.split('T');
    return dateArr[0];
}
function myCreateDateObject(dateStr, timeStr) {
    const date = myGetDate(dateStr);
    return new Date(date + ' ' + timeStr);
}

function dateObjectToTime(dateObject) {
    if (dateObject.getMinutes() == 0 && dateObject.getHours() / 10 < 1) {
        return `0${dateObject.getHours()}:0${dateObject.getMinutes()}`;
    } else if (dateObject.getMinutes() == 0) {
        return `${dateObject.getHours()}:0${dateObject.getMinutes()}`;
    } else if (dateObject.getHours() / 10 < 1) {
        return `0${dateObject.getHours()}:${dateObject.getMinutes()}`;
    }
    return `${dateObject.getHours()}:${dateObject.getMinutes()}`;
}

async function getItineraryData(id) {
    try {
        const data = await fetch(`/itinerary/${id}`);
        const itineraryData = await data.json();
        const ownerEle = document.querySelector('.owner');

        //console.log(data);

        if (data.status === 400) {
            alert(itineraryData.message);
            window.location = '/index.html';
        }

        document.querySelector('#itinerary-title').innerHTML =
            itineraryData.name.substring(0, 1).toUpperCase() +
            itineraryData.name.substring(1);

        const res = await fetch(`/user/${itineraryData.owner_id}`);
        const user = await res.json();

        //Set owner html
        ownerEle.innerHTML = user.profile_name;
        //Redirect to owner profile
        ownerEle.addEventListener('click', () => {
            window.location = `/profile.html?id=${itineraryData.owner_id}`;
        });
        vue.owner = user.profile_name;

        return {
            id: id,
            duration: itineraryData.days,
            startDate: itineraryData.start_date,
            ownerId: itineraryData.owner_id,
        };
    } catch (err) {
        console.log(err);
    }
}

async function getAllEvents(itineraryID, calendar) {
    try {
        const events = await fetch(`/event/all-events/${itineraryID}`);
        const eventsData = await events.json();

        //console.log(eventsData.rows);

        if (eventsData.rows[0]) {
            for (let event of eventsData.rows) {
                if (event.type === 'main') {
                    calendar.addEvent({
                        id: event.id,
                        title: event.name,
                        start: event.start_time,
                        end: event.end_time,
                        backgroundColor: 'rgb(55,136,216)',
                        borderColor: 'rgb(55,136,216)',
                    });
                } else if (event.type === 'transport') {
                    //console.log('here');
                    calendar.addEvent({
                        id: event.id,
                        title: event.name,
                        start: event.start_time,
                        end: event.end_time,
                        backgroundColor: 'rgb(51, 182, 121)',
                        borderColor: 'rgb(51, 182, 121)',
                    });
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
}

//Clone itinerary
document.querySelector('.clone').addEventListener('click', async (e) => {
    try {
        e.preventDefault();

        //Get itinerary data
        const res = await fetch(`/itinerary/${itineraryID}`);
        let data = await res.json();

        data.name += ' copy';
        data.public = false;

        //Copy to the viewer itinerary
        const clone = await fetch(`/itinerary`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify(data),
        });

        const cloneRes = await clone.json();

        // Get all events from current itinerary
        const eventGet = await fetch(`/event/all-events/${itineraryID}`);
        const events = await eventGet.json();

        for (let event of events.rows) {
            //console.log(event);

            const eventObject = calendar.getEventById(event.id);
            event.itinerary_id = cloneRes.id;

            event.start_time = `${myGetDate(eventObject.startStr)} ${myGetTime(
                eventObject.startStr
            )}`;

            //console.log(event.start_time);

            event.end_time = `${myGetDate(eventObject.endStr)} ${myGetTime(
                eventObject.endStr
            )}`;

            //console.log(event.end_time);

            //Copy all events to copied itinerary
            const copyEvents = await fetch(`/event/${itineraryID}`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                body: JSON.stringify(event),
            });

            //console.log(copyEvents);
        }

        //Get all destination from this itinerary
        const getDestination = await fetch(`/destination/${itineraryID}`);
        const desData = await getDestination.json();

        //console.log(desData);

        //Copy destination to the new itinerary
        const copyDestination = await fetch(`/destination/`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({
                itinerary_id: cloneRes.id,
                destinationArr: desData,
            }),
        });

        //console.log(copyDestination);

        //Add viewer as the member of the copied itinerary
        const createNewMember = await fetch('/itinerary/member', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({ itinerary_id: cloneRes.id }),
        });

        //const newMemberData = await createNewMember.json();

        window.location = `/itinerary_edit.html?id=${cloneRes.id}`;
    } catch (err) {
        console.error(err);
    }
});

async function changeDestinationTitle(id) {
    const res = await fetch(`/destination/${id}`);
    const destination = await res.json();

    const itineraryTitle = document.querySelector('.itinerary-title');

    itineraryTitle.innerHTML = '';

    for (let des of destination) {
        if (des.city === des.country) {
            const country = `<p>${des.country}</p>`;
            itineraryTitle.insertAdjacentHTML('beforeend', country);
        } else {
            const cityAndCountry = `<p>${des.city}, ${des.country}</p>`;
            itineraryTitle.insertAdjacentHTML('beforeend', cityAndCountry);
        }
    }
}
