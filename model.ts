export interface Itinerary {
    id?: number;
    name: string;
    city: string; // new added
    country: string; // new added
    days: number;
    startDate?: Date;
    ownerId: number;
    public: boolean;
}

export interface ItineraryEvent {
    id?: number;
    itinerary_id: number;
    name: string;
    type: 'main' | 'transport';
    startTime: Date;
    endTime: Date;
    location?: string;
    remark: string;
}

export interface ProfileSetting {
    id?: number;
    profileName: string;
    info?: string;
    interest: string;
    darkMode: boolean;
}

export interface newEvent {
    itinerary_id: number;
    name: string;
    type: string;
    start_time: string;
    end_time: string;
}
