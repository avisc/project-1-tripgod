const params = new URLSearchParams(window.location.search);
let itineraryID = params.get('id');
let itineraries = [];

window.onload = () => {
    renderEditor(itineraryID);
    checkMember(itineraryID);
    changeDestinationTitle(itineraryID);
    loadSetting();
};

let data = {
    darkMode: '',
};

// Vue app
const vue = new Vue({
    el: '#vue',
    data: {
        itineraries: itineraries,
        owner: '',
    },
    methods: {
        hello: function (str) {
            console.log(str);
        },
        loadItinerary: async function (id) {
            try {
                await checkMember(id);
                await changeDestinationTitle(id);
                socket.emit('leave-room', itineraryID);
                socket.emit('join-room', id);
                itineraryID = id;
                document.querySelector('#calendar').innerHTML = '';
                day = 1;
                itineraryData = await getItineraryData(id);
                calendarEl = document.getElementById('calendar');
                calendar = new FullCalendar.Calendar(calendarEl, setting);
                //Render all Events
                await getAllEvents(id, calendar);
                console.log(calendar.getEvents());
                calendar.render();
                renderEditor(id);
            } catch (err) {
                console.error(err);
            }
        },
    },
});

async function loadSetting() {
    const res = await fetch(`/profile`);
    const result = await res.json();
    data.profileName = result.profile_name;
    data.info = result.info;
    data.interest = result.interest;
    data.darkMode = result.dark_mode;
    if (!result.photo) {
        data.image = `/img/dummy_profilepic.jpg`;
    } else {
        data.image = `/upload/${result.photo}`;
    }
    // return;
    if (result.dark_mode) {
        document.body.classList.toggle('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

//TODO Edit page flashed when changed to view
async function checkMember(id) {
    try {
        const res = await fetch(`/itinerary/member/${id}`);

        if (res.status !== 200) {
            window.location = `/itinerary_view.html?id=${id}`;
        }
    } catch (err) {
        console.log(err);
    }
}

let itineraryData;
const socket = io.connect();
socket.emit('join-room', itineraryID);

// Fetch user itinerary list
async function fetchItineraryList() {
    try {
        const data = await fetch(`/itinerary/user/${itineraryData.ownerId}`);
        results = await data.json();
        for (let result of results) {
            itineraries.push(result);
        }
        return;
    } catch (err) {
        console.log(err);
    }
}

//DOM element
//Popup
const popup = document.querySelector('#popup');
const popupForm = document.querySelector('#popup-form');
const closePopupBtn = document.querySelector('.close-popup');

//View
const view = document.querySelector('#view');
const closeViewBtn = document.querySelectorAll('.close');
const deleteEventBtn = document.querySelector('.delete-event');

//Edit Page
const editEventBtn = document.querySelector('.edit-event');
const editEventPage = document.querySelector('#edit-event');
const editForm = document.querySelector('#edit-form');

//Store selected event ID
let selectedEventID;
//Store clicked event ID
let clickedEventID;
//For display day in content header
let day = 1;

let calendarEl;
let calendar;
let setting = {
    selectable: true,
    editable: true,
    height: '100%',
    //Turn of toolbar
    headerToolbar: false,
    initialView: 'timeGrid',
    //Duration and visibleRange will only take one
    //So it is better to use visibleRange with the duration calculated
    visibleRange: function () {
        let startDate = '2021-01-01';
        itineraryData.duration--;

        if (itineraryData.startDate !== null) {
            startDate = itineraryData.startDate;
            itineraryData.duration++;
        }

        endDate = new Date(startDate);
        endDate.setDate(new Date(startDate).getDate() + itineraryData.duration);

        return { start: startDate, end: endDate };
    },
    //Control header content
    dayHeaderContent: (args) => {
        if (itineraryData.startDate === null) {
            args.text = `Day ${day}`;
            day++;
        }
    },
    eventTimeFormat: {
        hour: 'numeric',
        minute: '2-digit',
        meridiem: true,
    },
    select: function (info) {
        const startTime = document.querySelectorAll('#start-time');
        const endTime = document.querySelectorAll('#end-time');

        //Get Date object of the event created
        const startDate = info.start;
        const endDate = info.end;

        //Get only the hh:mm of the event created
        let eventStartTime = myGetTime(info.startStr);
        let eventEndTime = myGetTime(info.endStr);

        //Set popup position next to event
        const domRect = info.jsEvent.target.getBoundingClientRect();

        if (domRect.y < 0) {
            domRect.y = window.innerHeight / 2;
        }
        if (domRect.y > window.innerHeight / 2) {
            domRect.y = window.innerHeight / 2;
        }

        popup.style.left = domRect.x + 'px';
        popup.style.top = domRect.y + 'px';

        //Setting the POPUP default value of start time and end time
        for (let time of startTime) {
            time.value = eventStartTime;
        }
        for (let time of endTime) {
            time.value = eventEndTime;
        }

        //Show popup
        popup.classList.toggle('popup-hidden');
        popup.classList.toggle('popup-show');

        const data = {
            itinerary_id: itineraryData.id,
            name: '(Untitled)',
            type: 'main',
            start_time: `${myGetDate(info.startStr)} ${eventStartTime}`,
            end_time: `${myGetDate(info.endStr)} ${eventEndTime}`,
        };

        socket.emit('new-event', data);
    },

    //Get date and time when dragged and dropped
    eventDrop: function (info) {
        try {
            //Update event with new date and time
            const calendarEvent = calendar.getEventById(info.event.id);

            // Update database
            const data = {
                itinerary_id: itineraryData.id,
                id: info.event.id,
                start_time: `${myGetDate(info.event.startStr)} ${myGetTime(
                    info.event.startStr
                )}`,
                end_time: `${myGetDate(info.event.endStr)} ${myGetTime(
                    info.event.endStr
                )}`,
            };

            socket.emit('update-event', data);
        } catch (err) {
            console.log(err);
        }
    },
    //Get date and time when resized
    eventResize: function (info) {
        try {
            const calendarEvent = calendar.getEventById(info.event.id);

            //Update event with new time
            const data = {
                itinerary_id: itineraryData.id,
                id: info.event.id,
                end_time: `${myGetDate(info.event.endStr)} ${myGetTime(
                    info.event.endStr
                )}`,
            };

            socket.emit('update-event', data);
        } catch (err) {
            console.log(err);
        }
    },
    //When event is clicked - view Detail
    eventClick: async function (info) {
        const title = document.querySelector('#view-title');
        const address = document.querySelector('#view-address');
        const remark = document.querySelector('#view-remarks');
        const startTime = document.querySelector('#view-start-time');
        const endTime = document.querySelector('#view-end-time');
        const from = document.querySelector('#view-from');

        const eventID = info.event.id;
        clickedEventID = info.event.id;

        const res = await fetch(`/event/${eventID}`);
        const data = await res.json();
        const eventData = data.rows[0];

        title.innerHTML = eventData.name;
        //Only show address and remarks if not equal to ""
        if (eventData.location === '') {
            address.parentElement.style.display = 'none';
            from.parentElement.style.display = 'none';
        }

        if (eventData.type === 'transport') {
            console.log(JSON.parse(eventData.location));
            if (
                JSON.parse(eventData.location).from === '' ||
                JSON.parse(eventData.location).to === ''
            ) {
                address.parentElement.style.display = 'none';
                from.parentElement.style.display = 'none';
            } else {
                address.parentElement.style.display = 'none';
                from.parentElement.style.display = 'initial';
            }
            const location = JSON.parse(eventData.location);
            from.innerHTML = location.from;
            document.querySelector('#view-to').innerHTML = location.to;
        } else {
            if (eventData.location === '') {
                address.parentElement.style.display = 'none';
            } else {
                address.parentElement.style.display = 'initial';
            }

            address.innerHTML = eventData.location;
        }

        if (!eventData.remark) {
            remark.parentElement.style.display = 'none';
        } else {
            remark.parentElement.style.display = 'initial';
            remark.innerHTML = eventData.remark;
        }

        const eventStartTime = dateObjectToTime(new Date(eventData.start_time));
        const eventEndTime = dateObjectToTime(new Date(eventData.end_time));

        startTime.innerHTML = eventStartTime;
        endTime.innerHTML = eventEndTime;

        view.classList.toggle('view-hidden');
        view.classList.toggle('view-show');

        //Set view position next to event
        const domRect = info.jsEvent.target.getBoundingClientRect();
        view.style.left = domRect.x + 'px';
        view.style.top = domRect.y + 'px';
    },
};

//Render calendar
document.addEventListener('DOMContentLoaded', reload);
async function reload() {
    itineraryData = await getItineraryData(itineraryID);
    fetchItineraryList(); // for left hand side itinerary list
    calendarEl = document.getElementById('calendar');
    calendar = new FullCalendar.Calendar(calendarEl, setting);
    //Render all Events
    await getAllEvents(itineraryID, calendar);

    calendar.render();
}

//Listen for new event created
socket.on('new-event', async (data) => {
    selectedEventID = data.id;

    const d = {
        id: data.id,
        title: data.name,
        start: data.start_time,
        end: data.end_time,
    };

    calendar.addEvent(d);
});

//Listen for event update
socket.on('update-event', (data) => {
    console.log('here');
    const eventObject = calendar.getEventById(data.id);

    for (let key in data) {
        switch (key) {
            case 'id':
                continue;
            case 'name':
                if (!data.name) continue;
                eventObject.setProp('title', data.name);
                break;
            case 'location':
                eventObject.setExtendedProp('address', data.location);
                break;
            case 'remark':
                eventObject.setExtendedProp('remarks', data.remark);
                break;
            case 'start_time':
                eventObject.setDates(
                    new Date(data.start_time),
                    eventObject.end
                );
                break;
            case 'end_time':
                eventObject.setDates(
                    eventObject.start,
                    new Date(data.end_time)
                );
            case 'type':
                if (data.type === 'main') {
                    eventObject.setProp('backgroundColor', 'rgb(55,136,216)');
                    eventObject.setProp('borderColor', 'rgb(55,136,216)');
                } else if (data.type === 'transport') {
                    eventObject.setProp('backgroundColor', 'rgb(51, 182, 121)');
                    eventObject.setProp('borderColor', 'rgb(51, 182, 121)');
                }
                break;
        }
    }
});

//Listen for delete event
socket.on('delete-event', (id) => {
    console.log(id);
    const eventObject = calendar.getEventById(id);
    eventObject.remove();
});

//Popup form submit
//Event form
const eventForm = document.querySelector('#event-form');
eventForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const inputArr = document.querySelectorAll(`#event-form input`);
    const remark = document.querySelector(`#event-form textarea`);

    //Get the event freshly created
    const calendarEvent = calendar.getEventById(selectedEventID);

    await updateEvent(selectedEventID, 'event-form', calendarEvent);

    popup.classList.toggle('popup-hidden');
    popup.classList.toggle('popup-show');

    //Clear input field
    for (let input of inputArr) {
        input.value = '';
    }
    remark.value = '';
});
//Transport form
const transportForm = document.querySelector('#transport-form');
transportForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const inputArr = document.querySelectorAll(`#transport-form input`);
    const remark = document.querySelector(`#transport-form textarea`);

    //Get the event freshly created
    const calendarEvent = calendar.getEventById(selectedEventID);

    await updateEvent(selectedEventID, 'transport-form', calendarEvent);

    popup.classList.toggle('popup-hidden');
    popup.classList.toggle('popup-show');

    //Clear input field
    for (let input of inputArr) {
        input.value = '';
    }
    remark.value = '';
});

//Close popup form and cancel event
closePopupBtn.addEventListener('click', async (e) => {
    e.preventDefault();

    const calendarEvent = calendar.getEventById(selectedEventID);

    await deleteEvent(selectedEventID, calendarEvent);

    popup.classList.toggle('popup-hidden');
    popup.classList.toggle('popup-show');
});

//Delete event
deleteEventBtn.addEventListener('click', async (e) => {
    e.preventDefault();

    const calendarEvent = calendar.getEventById(clickedEventID);

    await deleteEvent(clickedEventID, calendarEvent);

    view.classList.toggle('view-hidden');
    view.classList.toggle('view-show');
});

//Invite friend to co-edit
const inviteForm = document.querySelector('#invite-friend');
inviteForm.addEventListener('submit', async (e) => {
    try {
        e.preventDefault();

        //Get user ID typed in
        const userID = document.querySelector('#invite-friend #invite').value;

        //Stop if it is the owner ID
        if (userID === itineraryData.ownerID) {
            alert(`Can't Invite the owner!`);
            return;
        }

        //Stop if nothing is entered
        if (userID === '') {
            alert('Please enter a user ID!');
            return;
        }

        //Add new member to DB
        const res = await fetch(`/itinerary/member/${userID}`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({
                itinerary_id: itineraryID,
                ownerID: itineraryData.ownerId,
            }),
        });

        const result = await res.json();

        if (res.status === 400) {
            alert(result.message);
            return;
        }

        if (res.status === 401) {
            alert(result.message);
            window.location = '/index.html';
            return;
        }

        document.querySelector('#invite-friend #invite').value = '';

        renderEditor(itineraryID);
    } catch (err) {
        console.error(err);
    }
});

//Remove editor
const removeEditorForm = document.querySelector('#remove-editor');
removeEditorForm.addEventListener('submit', async (e) => {
    try {
        e.preventDefault();

        const userID = document.querySelector('#remove-editor #remove').value;

        if (userID === '') {
            alert('Please enter the user ID!');
            return;
        }

        const deleteMember = await fetch(`/itinerary/member/${userID}`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({
                ownerID: itineraryData.ownerId,
                itinerary_id: itineraryID,
            }),
        });

        const result = await deleteMember.json();

        if (deleteMember.status === 400) {
            alert(result.message);
            return;
        }

        if (deleteMember.status === 401) {
            alert(result.message);
            window.location = '/index.html';
            return;
        }

        document.querySelector('#remove-editor #remove').value = '';

        renderEditor(itineraryID);
    } catch (err) {
        console.error(err.message);
        alert(err.message);
    }
});

//Close view
for (let closeBtn of closeViewBtn) {
    closeBtn.addEventListener('click', (e) => {
        e.preventDefault();

        if (e.target.parentElement.id === 'edit-event') {
            editEventPage.classList.toggle('edit-hidden');
            editEventPage.classList.toggle('edit-show');
        }

        view.classList.toggle('view-hidden');
        view.classList.toggle('view-show');
    });
}

//Render edit event page
editEventBtn.addEventListener('click', async (e) => {
    console.log('here');

    const res = await fetch(`/event/${clickedEventID}`);
    const data = await res.json();
    const eventData = data.rows[0];

    console.log(eventData);

    const inputArr = document.querySelectorAll(`#edit-event-form input`);
    const remark = document.querySelector(`#edit-event-form textarea`);

    const eventStartTime = dateObjectToTime(new Date(eventData.start_time));
    const eventEndTime = dateObjectToTime(new Date(eventData.end_time));
    const inputArrTransport = document.querySelectorAll(
        `#edit-transport-form input`
    );

    const remarkTransport = document.querySelector(
        `#edit-transport-form textarea`
    );

    inputArr[0].value = inputArrTransport[0].value = eventData.name;
    inputArr[1].value = inputArrTransport[1].value = eventStartTime;
    inputArr[2].value = inputArrTransport[2].value = eventEndTime;
    remark.value = remarkTransport.value = eventData.remark;

    if (eventData.type === 'main') {
        inputArr[3].value = inputArrTransport[3].value = eventData.location;
        inputArrTransport[4].value = '';
    } else if (eventData.type === 'transport') {
        const location = JSON.parse(eventData.location);

        inputArr[3].value = inputArrTransport[3].value = location.from;
        inputArrTransport[4].value = location.to;
    }

    editEventPage.classList.toggle('edit-hidden');
    editEventPage.classList.toggle('edit-show');

    view.classList.toggle('view-hidden');
    view.classList.toggle('view-show');
});

// Submit edit event
const editEventForm = document.querySelector('#edit-event-form');
editEventForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    //Get the event clicked
    const calendarEvent = calendar.getEventById(clickedEventID);

    await updateEvent(clickedEventID, 'edit-event-form', calendarEvent);

    editEventPage.classList.toggle('edit-hidden');
    editEventPage.classList.toggle('edit-show');
});
const editTransportForm = document.querySelector('#edit-transport-form');
editTransportForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    //Get the event clicked
    const calendarEvent = calendar.getEventById(clickedEventID);

    await updateEvent(clickedEventID, 'edit-transport-form', calendarEvent);

    editEventPage.classList.toggle('edit-hidden');
    editEventPage.classList.toggle('edit-show');
});

//Clone itinerary
document.querySelector('.clone').addEventListener('click', async (e) => {
    try {
        e.preventDefault();

        //Get itinerary data
        const res = await fetch(`/itinerary/${itineraryID}`);
        let data = await res.json();

        data.name += ' copy';
        data.public = false;

        //Copy to the viewer itinerary
        const clone = await fetch(`/itinerary`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify(data),
        });

        const cloneRes = await clone.json();

        // Get all events from current itinerary
        const eventGet = await fetch(`/event/all-events/${itineraryID}`);
        const events = await eventGet.json();

        for (let event of events.rows) {
            console.log(event);

            const eventObject = calendar.getEventById(event.id);
            event.itinerary_id = cloneRes.id;

            event.start_time = `${myGetDate(eventObject.startStr)} ${myGetTime(
                eventObject.startStr
            )}`;

            console.log(event.start_time);

            event.end_time = `${myGetDate(eventObject.endStr)} ${myGetTime(
                eventObject.endStr
            )}`;

            console.log(event.end_time);

            //Copy all events to copied itinerary
            const copyEvents = await fetch(`/event/${itineraryID}`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                body: JSON.stringify(event),
            });

            console.log(copyEvents);
        }

        //Get all destination from this itinerary
        const getDestination = await fetch(`/destination/${itineraryID}`);
        const desData = await getDestination.json();

        console.log(desData);

        //Copy destination to the new itinerary
        const copyDestination = await fetch(`/destination/`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({
                itinerary_id: cloneRes.id,
                destinationArr: desData,
            }),
        });

        console.log(copyDestination);

        //Add viewer as the member of the copied itinerary
        const createNewMember = await fetch('/itinerary/member', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({ itinerary_id: cloneRes.id }),
        });

        const newMemberData = await createNewMember.json();

        window.location = `/itinerary_edit.html?id=${cloneRes.id}`;
    } catch (err) {
        console.error(err);
    }
});

//Edit Itinerary
document.querySelector('.edit').addEventListener('click', (e) => {
    try {
        e.preventDefault();

        window.location = `/edit.html?id=${itineraryID}`;
    } catch (err) {
        console.error(err);
    }
});

//Custom time and day functions
function myGetTime(dateStr) {
    const dateArr = dateStr.split('T');
    return dateArr[1].slice(0, 5);
}
function myGetDate(dateStr) {
    const dateArr = dateStr.split('T');
    return dateArr[0];
}
function myCreateDateObject(dateStr, timeStr) {
    const date = myGetDate(dateStr);
    return new Date(date + ' ' + timeStr);
}

function dateObjectToTime(dateObject) {
    if (dateObject.getMinutes() == 0 && dateObject.getHours() / 10 < 1) {
        return `0${dateObject.getHours()}:0${dateObject.getMinutes()}`;
    } else if (dateObject.getMinutes() == 0) {
        return `${dateObject.getHours()}:0${dateObject.getMinutes()}`;
    } else if (dateObject.getHours() / 10 < 1) {
        return `0${dateObject.getHours()}:${dateObject.getMinutes()}`;
    }
    return `${dateObject.getHours()}:${dateObject.getMinutes()}`;
}

async function updateEvent(id, formID, eventObject) {
    //Get all the input field (field should be the same for the same type of event)
    const inputArr = document.querySelectorAll(`#${formID} input`);
    const remark = document.querySelector(`#${formID} textarea`);

    //Get the event selected or clicked
    const calendarEvent = eventObject;

    if (formID === 'event-form' || formID === 'edit-event-form') {
        //Set event properties as the submitted value
        if (inputArr[0].value === '') {
            inputArr[0].value = '(Untitled)';
        }
        calendarEvent.setProp('title', inputArr[0].value);
        const start = myCreateDateObject(
            calendarEvent.startStr,
            inputArr[1].value
        );
        const end = myCreateDateObject(calendarEvent.endStr, inputArr[2].value);
        calendarEvent.setDates(start, end);
        calendarEvent.setExtendedProp('address', inputArr[3].value);
        calendarEvent.setExtendedProp('remarks', remark.value);
        calendarEvent.setProp('backgroundColor', 'rgb(55,136,216)');
        calendarEvent.setProp('borderColor', 'rgb(55,136,216)');

        //Update database
        const data = {
            itinerary_id: itineraryData.id,
            id: id,
            type: 'main',
            name: inputArr[0].value,
            location: inputArr[3].value,
            remark: remark.value,
            start_time: `${myGetDate(calendarEvent.startStr)} ${
                inputArr[1].value
            }`,
            end_time: `${myGetDate(calendarEvent.endStr)} ${inputArr[2].value}`,
        };

        socket.emit('update-event', data);
    } else if (
        formID === 'transport-form' ||
        formID === 'edit-transport-form'
    ) {
        //Set event properties as the submitted value
        if (inputArr[0].value === '') {
            inputArr[0].value = '(Untitled)';
        }
        calendarEvent.setProp('title', inputArr[0].value);
        const start = myCreateDateObject(
            calendarEvent.startStr,
            inputArr[1].value
        );
        const end = myCreateDateObject(calendarEvent.endStr, inputArr[2].value);
        calendarEvent.setDates(start, end);
        calendarEvent.setExtendedProp('address', {
            from: inputArr[3].value,
            to: inputArr[4].value,
        });
        calendarEvent.setExtendedProp('remarks', remark.value);
        calendarEvent.setProp('backgroundColor', 'rgb(51, 182, 121)');
        calendarEvent.setProp('borderColor', 'rgb(51, 182, 121)');

        //Update database
        const data = {
            itinerary_id: itineraryData.id,
            id: id,
            type: 'transport',
            name: inputArr[0].value,
            location: {
                from: inputArr[3].value,
                to: inputArr[4].value,
            },
            remark: remark.value,
            start_time: `${myGetDate(calendarEvent.startStr)} ${
                inputArr[1].value
            }`,
            end_time: `${myGetDate(calendarEvent.endStr)} ${inputArr[2].value}`,
        };

        socket.emit('update-event', data);
    }
}

async function deleteEvent(id, eventObject) {
    //remove event from calendar
    eventObject.remove();

    //remove event from DB
    socket.emit('delete-event', { id: id, itinerary_id: itineraryData.id });
}

const showRemoveBtn = document.querySelector('.remove');
const showAddBtn = document.querySelector('.invite');

async function getItineraryData(id) {
    try {
        const data = await fetch(`/itinerary/${id}`);
        const itineraryData = await data.json();
        const ownerEle = document.querySelector('.owner');

        if (data.status === 400) {
            alert(itineraryData.message);
            window.location = '/index.html';
        }

        document.querySelector('#itinerary-title').innerHTML =
            itineraryData.name.substring(0, 1).toUpperCase() +
            itineraryData.name.substring(1);

        const res = await fetch(`/user/${itineraryData.owner_id}`);
        const user = await res.json();

        //Set owner html
        ownerEle.innerHTML = user.profile_name;
        //Redirect to owner profile
        ownerEle.addEventListener('click', () => {
            window.location = `/profile.html?id=${itineraryData.owner_id}`;
        });
        vue.owner = user.profile_name;

        if (itineraryData.isOwner) {
            showAddBtn.style.display = 'initial';
            showRemoveBtn.style.display = 'initial';
        }

        return {
            id: id,
            duration: itineraryData.days,
            startDate: itineraryData.start_date,
            ownerId: itineraryData.owner_id,
        };
    } catch (err) {
        console.log(err);
    }
}

async function renderEditor(id) {
    const res = await fetch(`/itinerary/member/username/${id}`);

    const members = await res.json();

    const editorsField = document.querySelector('.editor');

    editorsField.innerHTML = '';
    for (let member of members) {
        editorsField.insertAdjacentHTML(
            'beforeend',
            `<p><span onclick="toProfile(${member.id})">${member.profile_name}</span></p>`
        );
    }
}

function toProfile(userID) {
    window.location = `/profile.html?id=${userID}`;
}

async function getAllEvents(itineraryID, calendar) {
    try {
        const events = await fetch(`/event/all-events/${itineraryID}`);
        const eventsData = await events.json();

        if (eventsData.rows[0]) {
            for (let event of eventsData.rows) {
                if (event.type === 'main') {
                    calendar.addEvent({
                        id: event.id,
                        title: event.name,
                        start: event.start_time,
                        end: event.end_time,
                    });
                } else if (event.type === 'transport') {
                    calendar.addEvent({
                        id: event.id,
                        title: event.name,
                        start: event.start_time,
                        end: event.end_time,
                        backgroundColor: 'rgb(51, 182, 121)',
                        borderColor: 'rgb(51, 182, 121)',
                    });
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
}

async function changeDestinationTitle(id) {
    const res = await fetch(`/destination/${id}`);
    const destination = await res.json();

    const itineraryTitle = document.querySelector('.itinerary-title');

    itineraryTitle.innerHTML = '';

    for (let des of destination) {
        if (des.city === des.country) {
            const country = `<p>${des.country}</p>`;
            itineraryTitle.insertAdjacentHTML('beforeend', country);
        } else {
            const cityAndCountry = `<p>${des.city}, ${des.country}</p>`;
            itineraryTitle.insertAdjacentHTML('beforeend', cityAndCountry);
        }
    }
}

showAddBtn.addEventListener('click', (e) => {
    e.preventDefault();

    document.querySelector('.invite-form').classList.toggle('hidden');
});

showRemoveBtn.addEventListener('click', (e) => {
    e.preventDefault();

    document.querySelector('.editor-form').classList.toggle('hidden');
});
