window.onload = () => {
    loadSetting();
    loadItinerary();
};
const params = new URLSearchParams(window.location.search);
const id = params.get('id');
if (!id) {
    window.location = '/';
}

let data = {
    name: '',
    days: '',
    startDate: '',
    public: '',
    darkMode: '',
};

// Vue app
const app = new Vue({
    el: '#vue',
    data: data,
    methods: { updateItinerary },
});

async function loadSetting() {
    const res = await fetch(`/profile`);
    const result = await res.json();
    data.darkMode = result.dark_mode;
    if (result.dark_mode) {
        document.body.classList.toggle('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

const resultDiv = document.querySelector('#result-container');
let destinationArr = [];
let oldDesArr = [];
let dataID = 1;

// Fetch itinerary info data and render on the form
async function loadItinerary() {
    const checkMember = await fetch(`/itinerary/member/${id}`);
    if (checkMember.status !== 200) {
        const err = await checkMember.json();
        alert(err.message);
        window.location = '/';
        return;
    }
    const res = await fetch(`/itinerary/${id}`);
    const result = await res.json();
    const startDate = result.start_date
        ? moment(result.start_date).format('YYYY-MM-DD')
        : null;
    data.name = result.name;
    data.days = result.days;
    data.startDate = startDate;
    if (result.public) {
        data.public = '1';
    } else {
        data.public = '0';
    }

    //Get destination of an itinerary
    const desData = await fetch(`/destination/${id}`);
    const destinations = await desData.json();
    console.log(destinations);
    //Add destination to the result div
    for (let destination of destinations) {
        const result = `<p data-id ="${dataID}">${destination.city}, ${destination.country} <i class="fas fa-times-circle" onclick=deleteDestination(${dataID})></i></p>`;
        resultDiv.insertAdjacentHTML('beforeend', result);

        destinationArr.push({
            id: dataID,
            country: destination.country,
            city: destination.city,
        });

        oldDesArr.push({
            id: dataID,
            country: destination.country,
            city: destination.city,
        });

        dataID++;
    }
}

// Update itinerary info to database
async function updateItinerary() {
    const res = await fetch(`/itinerary/${id}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        body: JSON.stringify(data),
    });
    if (res.status !== 200) {
        const err = await res.json();
        alert(err.message);
        window.location = '/';
    }

    //For Update destination
    if (destinationArr.length > 0 && res.status === 200) {
        let compareArr = [];
        for (let oldDes of oldDesArr) {
            destinationArr = destinationArr.filter((des) => {
                if (
                    des.country === oldDes.country &&
                    des.city === oldDes.city
                ) {
                    compareArr.push(des);
                }
                return (
                    des.country !== oldDes.country && des.city !== oldDes.city
                );
            });
        }

        //console.log(compareArr);
        //console.log(oldDesArr);

        if (oldDesArr.length !== compareArr.length) {
            for (let arr of compareArr) {
                oldDesArr = oldDesArr.filter((des) => {
                    return des.country !== arr.country && des.city !== arr.city;
                });
            }

            oldDesArr = oldDesArr.map((obj) => {
                return {
                    city: obj.city,
                    country: obj.country,
                };
            });

            // Delete previous destination
            const deleteDes = await fetch(`/destination/${id}`, {
                method: 'DELETE',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                body: JSON.stringify(oldDesArr),
            });
        }

        const newArr = destinationArr.map((obj) => {
            return {
                city: obj.city,
                country: obj.country,
            };
        });

        //Add new destination
        const newDes = await fetch(`/destination`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({ destinationArr: newArr, itinerary_id: id }),
        });
    }

    if (res.status === 200) {
        window.location = `/itinerary_edit.html?id=${id}`;
    } else {
        const response = await res.json();
        alert(response.error);
    }
}

var key = 'pk.fc8f8c7f253fe0b58c140cb34ad2c4f5';

// Initialize an empty map without layers (invisible map)
var map = L.map('map', {
    center: [40.7259, -73.9805], // Map loads with this location as center
    zoom: 12,
    scrollWheelZoom: true,
    zoomControl: false,
    attributionControl: false,
});

//Geocoder options
var geocoderControlOptions = {
    bounds: false, //To not send viewbox
    markers: false, //To not add markers when we geocoder
    attribution: null, //No need of attribution since we are not using maps
    expanded: true, //The geocoder search box will be initialized in expanded mode
    panToPoint: false, //Since no maps, no need to pan the map to the geocoded-selected location
    params: {
        //Set dedupe parameter to remove duplicate results from Autocomplete
        dedupe: 1,
    },
};

//Initialize the geocoder
var geocoderControl = new L.control.geocoder(
    'pk.fc8f8c7f253fe0b58c140cb34ad2c4f5',
    geocoderControlOptions
)
    .addTo(map)
    .on('select', function (e) {
        const country = e.feature.feature.address.country;
        const city = e.feature.feature.address.name;
        const lat = e.latlng.lat;
        const lng = e.latlng.lng;

        const result = `<p data-id ="${dataID}">${e.feature.feature.display_name} <i class="fas fa-times-circle" onclick=deleteDestination(${dataID})></i></p>`;
        resultDiv.insertAdjacentHTML('beforeend', result);

        destinationArr.push({
            id: dataID,
            country: e.feature.feature.address.country,
            city: e.feature.feature.address.name,
        });

        dataID++;
        geocoderControl.reset();

        console.log(destinationArr);
    });

//Get the "search-box" div
var searchBoxControl = document.getElementById('search-box');
//Get the geocoder container from the leaflet map
var geocoderContainer = geocoderControl.getContainer();
//Append the geocoder container to the "search-box" div
searchBoxControl.appendChild(geocoderContainer);

function deleteDestination(index) {
    const arrIndex = destinationArr
        .map((obj) => {
            return obj.id;
        })
        .indexOf(index);
    destinationArr.splice(arrIndex, 1);
    console.log(destinationArr);

    const ele = document.querySelector(`[data-id='${index}']`);
    ele.remove();
}
