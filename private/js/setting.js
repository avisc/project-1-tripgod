window.onload = () => {
    loadSetting();
};

let data = {
    profileName: '',
    info: '',
    interest: [],
    darkMode: false,
    image: '',
};

// Vue app
const app = new Vue({
    el: '#vue',
    data: data,
    methods: { saveSetting },
});

// Dark Mode
let darkModeOn = document.querySelector('input#dark');
darkModeOn.addEventListener('change', function () {
    if (darkModeOn.value === 'dark') {
        var element = document.body;
        element.classList.toggle('dark');
    } else {
        element.classList.remove('dark');
    }
});

// Fetch profile info data and render on the form
async function loadSetting() {
    const res = await fetch(`/profile`);
    const result = await res.json();
    data.profileName = result.profile_name;
    data.info = result.info;
    data.interest = result.interest;
    data.darkMode = result.dark_mode;
    if (!result.photo) {
        data.image = `/img/dummy_profilepic.jpg`;
    } else {
        data.image = `/upload/${result.photo}`;
    }
    // Dark Mode
    if (result.dark_mode === true) {
        var element = document.body;
        element.classList.toggle('dark');
    } else {
        element.classList.remove('dark');
    }
}

// Update profile info to database
async function saveSetting(event) {
    const form = event.target;
    const formData = new FormData();

    formData.append('profileName', data.profileName);
    formData.append('info', data.info);
    formData.append('interest', JSON.stringify(data.interest));
    formData.append('darkMode', data.darkMode);
    formData.append('profile', form.profile.files[0]);
    const res = await fetch(`/profile/`, {
        method: 'PUT',
        body: formData,
    });
    const response = await res.json();
    if (res.status !== 200) {
        alert(data.message);
    } else {
        window.location = `/profile.html`;
    }
}
