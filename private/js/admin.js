window.onload = () => {
    loadSetting();
    removeItinerary();
    removeUser();
    checkAdmin();
};

let data = {
    profileName: '',
    info: '',
    interest: [],
    darkMode: '',
    image: '',
};

async function loadSetting() {
    const res = await fetch(`/profile`);
    const result = await res.json();
    data.profileName = result.profile_name;
    data.info = result.info;
    data.interest = result.interest;
    data.darkMode = result.dark_mode;
    if (!result.photo) {
        data.image = `/img/dummy_profilepic.jpg`;
    } else {
        data.image = `/upload/${result.photo}`;
    }
    // Dark Mode
    if (result.dark_mode === true) {
        document.body.classList.toggle('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

// Remove Itinerary
function removeItinerary() {
    const admin = document.getElementById('admin-itin');

    admin.addEventListener('submit', async (event) => {
        //console.log('admin logged in');

        try {
            event.preventDefault();

            const removeItin = document.getElementById('removeitinerary').value;
            //console.log(`Itinerary ID: ${removeItin} has been removed.`);

            if (!removeItin) {
                return;
            } else {
                const res = await fetch(`/itinerary/${removeItin}`, {
                    method: 'DELETE',
                });
                alert('Itinerary removed!');
            }
        } catch (err) {
            console.error(err);
            alert(err.message);
        }
    });
}

// Remove User
function removeUser() {
    const admin = document.getElementById('admin-user');

    admin.addEventListener('submit', async (event) => {
        console.log('admin logged in');

        try {
            event.preventDefault();

            const removeUser = document.getElementById('removeuser').value;
            console.log(`User ID: ${removeUser} has been removed.`);

            if (!removeUser) {
                //??
                return;
            } else {
                const res = await fetch(`/profile/${removeUser}`, {
                    method: 'DELETE',
                });
                alert('User removed!');
            }
        } catch (err) {
            console.error(err);
            alert(err.message);
        }
    });
}

async function checkAdmin() {
    console.log('check admin');
    const res = await fetch('/admin');
    adminInfo = await res.json();
    console.log(adminInfo);

    if (!adminInfo.id) {
        window.location = '/index.html';
    } else {
        console.log('is Admin');
    }
}
