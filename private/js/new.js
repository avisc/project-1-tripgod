window.onload = () => {
    loadSetting();
    createItinerary();
};

let data = {
    darkMode: '',
};

async function loadSetting() {
    const res = await fetch(`/profile`);
    const result = await res.json();
    data.darkMode = result.dark_mode;
    // return;
    if (result.dark_mode === true) {
        document.body.classList.toggle('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

var key = 'pk.fc8f8c7f253fe0b58c140cb34ad2c4f5';

// Initialize an empty map without layers (invisible map)
var map = L.map('map', {
    center: [40.7259, -73.9805], // Map loads with this location as center
    zoom: 12,
    scrollWheelZoom: true,
    zoomControl: false,
    attributionControl: false,
});

//Geocoder options
var geocoderControlOptions = {
    bounds: false, //To not send viewbox
    markers: false, //To not add markers when we geocoder
    attribution: null, //No need of attribution since we are not using maps
    expanded: true, //The geocoder search box will be initialized in expanded mode
    panToPoint: false, //Since no maps, no need to pan the map to the geocoded-selected location
    params: {
        //Set dedupe parameter to remove duplicate results from Autocomplete
        dedupe: 1,
    },
};

const destinationArr = [];
const resultDiv = document.querySelector('#result-container');
let dataID = 1;

//Initialize the geocoder
var geocoderControl = new L.control.geocoder(
    'pk.fc8f8c7f253fe0b58c140cb34ad2c4f5',
    geocoderControlOptions
)
    .addTo(map)
    .on('select', function (e) {
        const country = e.feature.feature.address.country;
        const city = e.feature.feature.address.name;
        const lat = e.latlng.lat;
        const lng = e.latlng.lng;

        const result = `<p data-id ="${dataID}">${e.feature.feature.display_name} <i class="fas fa-times-circle" onclick=deleteDestination(${dataID})></i></p>`;
        resultDiv.insertAdjacentHTML('beforeend', result);

        destinationArr.push({
            id: dataID,
            country: e.feature.feature.address.country,
            city: e.feature.feature.address.name,
        });

        dataID++;
        geocoderControl.reset();
    });

//Get the "search-box" div
var searchBoxControl = document.getElementById('search-box');
//Get the geocoder container from the leaflet map
var geocoderContainer = geocoderControl.getContainer();
//Append the geocoder container to the "search-box" div
searchBoxControl.appendChild(geocoderContainer);

// Create new itinerary on submit
function createItinerary() {
    const form = document.querySelector('#create-itinerary');
    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        //Get title of itinerary
        const name = form.title.value;

        //Get Duration
        const days = Number(form.days.value);
        //Get Start date
        let startDate = form['start-date'].value;
        if (startDate === '') {
            startDate = null;
        }
        //Get visibility
        const public = form.public.value;

        const newItinerary = { name, days, startDate, public };
        console.log(newItinerary);

        //Create new itinerary
        const res = await fetch('/itinerary', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify(newItinerary),
        });
        const response = await res.json();
        console.log(response);

        //Add owner as new member
        const createNewMember = await fetch('/itinerary/member', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: JSON.stringify({ itinerary_id: response.id }),
        });

        const data = await createNewMember.json();

        const newArr = destinationArr.map((obj) => {
            return {
                city: obj.city,
                country: obj.country,
            };
        });

        //Link destination to itinerary
        if (destinationArr.length > 0) {
            const newDestination = await fetch('/destination', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                body: JSON.stringify({
                    destinationArr: newArr,
                    itinerary_id: response.id,
                }),
            });

            const destinationResult = await newDestination.json();

            console.log(newDestination);
            console.log(destinationResult);
        }

        if (res.status !== 500) {
            window.location = `/itinerary_edit.html?id=${response.id}`;
        } else {
            alert(res.message);
        }
    });
}

function deleteDestination(index) {
    const arrIndex = destinationArr
        .map((obj) => {
            return obj.id;
        })
        .indexOf(index);
    destinationArr.splice(arrIndex, 1);
    console.log(destinationArr);

    const ele = document.querySelector(`[data-id='${index}']`);
    ele.remove();
}
