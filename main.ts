import express from 'express';
import { Request, Response, NextFunction } from 'express';
import path from 'path';
import expressSession from 'express-session';
import pg from 'pg';
import dotenv from 'dotenv';
import grant from 'grant';
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import { newEvent } from './model';

dotenv.config();

//Connect to Database
export const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
});

client.connect();

//Use Session
const sessionMiddleware = expressSession({
    secret: 'C13 Group 1',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false },
});

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(sessionMiddleware);

// Initialize socket.io server
const server = new http.Server(app);
export const io = new SocketIO(server);

io.use((socket, next) => {
    const request = socket.request as express.Request;
    sessionMiddleware(
        request,
        request.res as express.Response,
        next as express.NextFunction
    );
});

io.on('connection', function (socket) {
    console.log(`[info] ${socket.id} is connected.`);

    socket.on('join-room', async (itineraryID: number) => {
        console.log(itineraryID);

        if (socket.request.session['user']) {
            try {
                console.log(socket.request.session['user']);

                //Check if it is the member of this itinerary
                const query =
                    'SELECT * FROM itinerary_member WHERE user_id = $1 AND itinerary_id = $2';

                const result = await client.query(query, [
                    socket.request.session['user'].id,
                    itineraryID,
                ]);

                const userItinerary = result.rows[0];

                //Don't allow to join room if they already in a room

                const itineraryNumber = `Itinerary-${userItinerary.itinerary_id}`;
                socket.join(itineraryNumber);
                console.log(
                    `User ID: ${socket.request.session['user'].id} joined Itinerary: ${itineraryNumber}`
                );
            } catch (err) {
                console.error(err);
            }
        }

        console.log(socket.rooms);
    });

    socket.on('new-event', async (data: newEvent) => {
        const query =
            'INSERT INTO itinerary_event(itinerary_id, name, type, start_time,end_time) VALUES ($1, $2, $3, $4, $5) RETURNING id';
        const eventID = await client.query(query, [
            data.itinerary_id,
            data.name,
            data.type,
            data.start_time,
            data.end_time,
        ]);

        console.log(data.itinerary_id);

        io.to(`Itinerary-${data.itinerary_id}`).emit('new-event', {
            id: eventID.rows[0].id,
            ...data,
        });
    });

    socket.on('update-event', async (data: any) => {
        console.log('here');

        const id = data.id;
        let order = 1;
        let query = 'UPDATE itinerary_event SET ';
        let queryArr = [];
        let returning = ' RETURNING ';

        for (let key in data) {
            if (key === 'id' || key === 'itinerary_id') {
                continue;
            }
            query += `${key} = $${order}, `;
            returning += `${key}, `;
            order++;
            queryArr.push(data[key]);
        }

        query =
            query.slice(0, query.length - 2) +
            ` WHERE id = $${order}` +
            returning +
            'id';

        queryArr.push(id);

        const result = await client.query(query, queryArr);

        console.log(result);
        console.log(data.itinerary_id);

        socket
            .to(`Itinerary-${data.itinerary_id}`)
            .emit('update-event', result.rows[0]);
    });

    socket.on('delete-event', async (data: any) => {
        const query = 'DELETE FROM itinerary_event WHERE id = $1';

        await client.query(query, [data.id]);

        socket
            .to(`Itinerary-${data.itinerary_id}`)
            .emit('delete-event', data.id);
    });

    socket.on('leave-room', (id: number) => {
        const itineraryNumber = `Itinerary-${id}`;
        socket.leave(itineraryNumber);
        console.log(
            `User ID: ${socket.request.session['user'].id} left Itinerary: ${itineraryNumber}`
        );
    });

    socket.on('disconnect', () => {
        console.log(`[info] ${socket.id} is disconnected`);
    });
});

//Grant for OAuth login
const grantExpress = grant.express({
    defaults: {
        origin: 'http://localhost:8080',
        transport: 'session',
        state: true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID || '',
        secret: process.env.GOOGLE_CLIENT_SECRET || '',
        scope: ['profile', 'email'],
        callback: '/login/google',
    },
});

app.use(grantExpress as express.RequestHandler);

// Route Handling
export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session?.['user']) {
        next();
    } else {
        res.redirect('/');
    }
}

import { userRoutes } from './routers/userRoutes';
app.use('/user', userRoutes);

import { loginRoutes } from './routers/loginRoutes';
app.use('/login', loginRoutes);

import { registerRoutes } from './routers/registerRoutes';
app.use('/register', registerRoutes);

import { eventRoutes } from './routers/eventRoutes';
app.use('/event', eventRoutes);

import { itineraryRoutes } from './routers/itineraryRoutes';
app.use('/itinerary', itineraryRoutes);

import { destinationRoutes } from './routers/destinationRoutes';
app.use('/destination', destinationRoutes);

import { profileRoutes } from './routers/profileRoutes';
app.use('/profile', profileRoutes);

import { adminRoutes } from './routers/adminRoutes';
app.use('/admin', adminRoutes);

//Use static file
app.use(express.static(path.join(__dirname, 'public')));
app.use(isLoggedIn, express.static(path.join(__dirname, 'private')));

// HTTP Method: GET, localhost:8080
app.get('/test', (req: Request, res: Response) => {
    res.end('Hello C13!');
});

app.post('/test', (req: Request, res: Response) => {
    res.json({ body: req.body });
});

// Redirect to 404.html for non-existing path
app.use((req: Request, res: Response) => {
    res.redirect('/404.html');
});

// Bind to the PORT
const PORT = 8080;
server.listen(PORT, () => {
    console.log(`listening to localhost:${PORT}`);
});
